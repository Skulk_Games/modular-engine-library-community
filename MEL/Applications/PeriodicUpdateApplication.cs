﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace Skulk.MEL
{
  /// <summary>
  /// Class that implements an application that has a regular application thread as well as a periodic update method.
  /// </summary>
  public class PeriodicUpdateApplication : ThreadedApplication
  {
    private IUpdateGenerator m_updateGenerator;

    /// <summary>
    /// Gets whether the update thread is running or not 
    /// </summary>
    public bool IsUpdateThreadRunning { get => m_updateGenerator.Running; }

    /// <summary>
    /// The maximum rate that update methods will be invoked.
    /// </summary>
    /// <remarks>
    /// If set while the update generator is already running it will be stopped updated and restarted
    /// </remarks>
    public double MaxUpdateHz { get => m_updateGenerator.MaxUpdateHz; set => SetMaxUpdateHz(value); }

    /// <summary>
    /// Event callback invoked when a periodic update is due
    /// </summary>
    public event UpdateCallback OnUpdate { add => m_updateGenerator.OnUpdate += value; remove => m_updateGenerator.OnUpdate -= value; }

    /// <summary>
    /// Initializes a new instance of the <see cref="PeriodicUpdateApplication"/> class with the specified max update frequency.
    /// </summary>
    /// <param name="maxUpdateHz">The maximum update frequency in Hz.</param>
    /// <param name="updateGenerator">The update generator instance to use, if null a new <see cref="PeriodicUpdateGenerator"/> instance will be used.</param>
    public PeriodicUpdateApplication(double maxUpdateHz = 10, IUpdateGenerator updateGenerator = null)
    {
      if (updateGenerator == null)
      {
        m_updateGenerator = new PeriodicUpdateGenerator(maxUpdateHz);
      }
      else
      {
        m_updateGenerator = updateGenerator;
      }
    }

    /// <summary>
    /// Starts the application run and update threads
    /// </summary>
    /// <param name="backgroundRun">Indicates whether the run thread is a background thread</param>
    public override void Start(bool backgroundRun)
    {
      base.Start(backgroundRun);
      m_updateGenerator.Start(true);
    }

    /// <summary>
    /// Sets the stop flag, causing the application run and update threads to stop
    /// </summary>
    public override void Stop()
    {
      base.Stop();
      m_updateGenerator.Stop();
    }

    /// <summary>
    /// Blocks waiting to join the run and update threads.
    /// </summary>
    public override void Join()
    {
      base.Join();
      m_updateGenerator.Join();
    }

    /// <summary>
    /// Blocks the calling thread until the application threads exit or the timeout elapses
    /// </summary>
    /// <param name="millisecondTimeout">The max time in milliseconds to wait for the threads to exit</param>
    /// <returns>True if all threads ended, false otherwise</returns>
    public override bool Join(int millisecondTimeout)
    {
      bool ended = base.Join(millisecondTimeout);
      return m_updateGenerator.Join(millisecondTimeout) && ended;
    }

    /// <summary>
    /// Sets the max update rate for the update generator.
    /// </summary>
    /// <param name="maxUpdateHz">The max update frequency to set</param>
    private void SetMaxUpdateHz(double maxUpdateHz)
    {
      m_updateGenerator.MaxUpdateHz = maxUpdateHz;
    }
  }
}
