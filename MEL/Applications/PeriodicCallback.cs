﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2020  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;

namespace Skulk.MEL
{
  /// <summary>
  /// A class meant to manage a callback that should periodically be invoked within the update loop
  /// of a <see cref="PeriodicUpdateApplication"/>.
  /// </summary>
  public class PeriodicCallback
  {
    private double m_period;
    private double m_time;
    private Action m_action;
    private Action<object> m_argAction;
    private object m_arg;
    private ILogger m_log;

    private PeriodicCallback(double period = 10, ILogger log = null)
    {
      m_period = period;
      m_log = log;
    }

    /// <summary>
    /// Initializes a void periodic callback
    /// </summary>
    /// <param name="action">The action to periodically invoke</param>
    /// <param name="period">The period in seconds that the event should be invoked</param>
    /// <param name="log">The log instance to use</param>
    public PeriodicCallback(Action action, double period = 10, ILogger log = null) : this(period, log)
    {
      m_action = action;
    }

    /// <summary>
    /// Initializes a periodic callback with a single argument
    /// </summary>
    /// <param name="argAction">The action to periodically invoke</param>
    /// <param name="arg">The argument object to pass to the periodic action</param>
    /// <param name="period">The period in seconds that the event should be invoked</param>
    /// <param name="log">The log instance to use</param>
    public PeriodicCallback(Action<object> argAction, ref object arg, double period = 10, ILogger log = null) : this(period, log)
    {
      m_argAction = argAction;
      m_arg = arg;
    }

    /// <summary>
    /// The periodic update method
    /// </summary>
    /// <param name="deltaTime">The time in seconds since this method was last called</param>
    public void Update(double deltaTime)
    {
      m_time += deltaTime;
      if (m_time >= m_period)
      {
        m_time -= m_period;
        if (m_time >= m_period)
        {
          m_log?.LogWarning("Periodic callback elapsed time is more than twice the period, update resolution may not be sufficient. Resetting elapsed time to 0.");
          m_time = 0;
        }

        try
        {
          if (m_argAction != null)
          {
            m_argAction.Invoke(m_arg);
          }
          else if (m_action != null)
          {
            m_action.Invoke();
          }
        }
        catch (Exception ex)
        {
          // Exception in action callback
          m_log?.LogError("Exception in periodic callback: {0}", ex.Message);
          m_log?.LogTrace(ex.ToString());
        }
      }
    }
  }
}
