﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

namespace Skulk.MEL
{
  /// <summary>
  /// A specialization of the <see cref="GenericModularApplication{ModuleParentType}"/> class that supports modules with a <see cref="ModularApplication"/> parent.
  /// </summary>
  public class GenericModularApplication<T> : ModularApplication
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="ModularApplication"/> class.
    /// </summary>
    public GenericModularApplication()
    {
      // Empty
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ModularApplication"/> class with the specified update rate.
    /// </summary>
    /// <param name="maxUpdateHz">The maximum rate that update events will be generated.</param>
    /// <param name="log">The log instance to use on this application.</param>
    /// <param name="updateGenerator">The class instance to use for generating periodic updates, if null a <see cref="PeriodicUpdateGenerator"/> instance will be used.</param>
    public GenericModularApplication(double maxUpdateHz, ILogger log = null, IUpdateGenerator updateGenerator = null) : base(maxUpdateHz, log, updateGenerator)
    {
      // Empty
    }
  }
}
