﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2020  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

namespace Skulk.MEL
{
  /// <summary>
  /// An update event callback.
  /// </summary>
  /// <param name="deltaTime">The time in seconds since the update was last called.</param>
  public delegate void UpdateCallback(UpdateTime deltaTime);

  /// <summary>
  /// An interface for a class that generates periodic updates
  /// </summary>
  public interface IUpdateGenerator
  {
    /// <summary>
    /// Gets the flag indicating if this update generator is running
    /// </summary>
    bool Running { get; }

    /// <summary>
    /// Gets or sets the max rate that update methods will be called
    /// </summary>
    double MaxUpdateHz { get; set; }

    /// <summary>
    /// The event callback invoked for each periodic update
    /// </summary>
    event UpdateCallback OnUpdate;

    /// <summary>
    /// Starts the update generator
    /// </summary>
    /// <param name="background">Indicates if the update generator thread should run as a background thread</param>
    void Start(bool background = true);

    /// <summary>
    /// Sets the running flag to false, causing the update thread to stop
    /// </summary>
    void Stop();

    /// <summary>
    /// Blocks waiting for the update thread to end
    /// </summary>
    void Join();

    /// <summary>
    /// Blocks waiting to join the update thread up to a max timeout
    /// </summary>
    /// <param name="milliseconds">The max time in milliseconds to wait for the update thread to end</param>
    /// <returns>True if the update thread ended, false if a timeout occurred</returns>
    bool Join(int milliseconds);
  }
}
