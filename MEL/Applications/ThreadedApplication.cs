﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Skulk.MEL
{
  /// <summary>
  /// A simple base class for an application that starts, stops and joins a running thread.
  /// </summary>
  public class ThreadedApplication
  {
    private volatile bool m_stopRun = false;
    private Thread m_appThread = null;

    /// <summary>
    /// Gets the flag indicating if the application thread should stop.
    /// </summary>
    public bool StopRun { get => m_stopRun; }

    /// <summary>
    /// Gets the flag indicating if the application thread is currently running
    /// </summary>
    public bool Running { get => m_appThread?.IsAlive ?? false; }

    /// <summary>
    /// Initializes a new instance of the <see cref="ThreadedApplication"/> class with default values.
    /// </summary>
    public ThreadedApplication()
    {
      // Empty
    }

    /// <summary>
    /// Starts this application's run thread. If the thread is already running it will be restarted.
    /// </summary>
    /// <param name="background">If true the run thread will be configured as a background thread</param>
    public virtual void Start(bool background = false)
    {
      if (m_appThread != null)
      {
        Stop();
        Join();
      }

      m_stopRun = false;
      m_appThread = new Thread(Run)
      {
        IsBackground = background
      };
      m_appThread.Start();
    }

    /// <summary>
    /// Sets this application's stop run flag to true so the run thread will exit.
    /// </summary>
    public virtual void Stop()
    {
      m_stopRun = true;
    }

    /// <summary>
    /// Blocks the calling thread until this application's run thread exits.
    /// </summary>
    public virtual void Join()
    {
      m_appThread?.Join();
      m_appThread = null;
    }

    /// <summary>
    /// Blocks the calling thread until this application's run thread exits or the specified time elapses.
    /// </summary>
    /// <param name="millisecondTimeout">The max number of milliseconds to wait for the run thread to exit.</param>
    /// <returns>True if the thread has terminated, false otherwise</returns>
    public virtual bool Join(int millisecondTimeout)
    {
      return m_appThread.Join(millisecondTimeout);
    }

    /// <summary>
    /// Calls <see cref="Thread.Interrupt()"/> on the run thread
    /// </summary>
    public virtual void Interrupt()
    {
      m_appThread?.Interrupt();
    }

    /// <summary>
    /// This application's run thread, does nothing unless overridden by derived classes.
    /// </summary>
    protected virtual void Run()
    {
      // Empty
    }
  }
}
