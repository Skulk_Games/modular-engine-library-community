﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Diagnostics;
using System.Threading;

namespace Skulk.MEL
{
  /// <summary>
  /// A class that generates periodic calls at a max specified rate
  /// </summary>
  public class PeriodicUpdateGenerator : IUpdateGenerator
  {
    private volatile bool m_running = false;
    private bool m_background = false;
    private double m_maxUpdateHz = 60;
    private Thread m_updateThread;
    private UpdateTime m_updateTime;

    /// <summary>
    /// Gets the flag indicating if this update generator is running
    /// </summary>
    public bool Running { get => m_running; }

    /// <summary>
    /// Gets or sets the max rate that update methods will be called
    /// </summary>
    public double MaxUpdateHz { get => m_maxUpdateHz; set => SetMaxUpdate(value); }

    /// <summary>
    /// The event callback invoked for each periodic update
    /// </summary>
    public event UpdateCallback OnUpdate;

    /// <summary>
    /// Default constructor
    /// </summary>
    public PeriodicUpdateGenerator()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this update generator instance with the specified max rate
    /// </summary>
    /// <param name="maxUpdateHz">The max rate to invoke the update callback</param>
    public PeriodicUpdateGenerator(double maxUpdateHz)
    {
      m_maxUpdateHz = maxUpdateHz;
    }

    /// <summary>
    /// Starts the update generator
    /// </summary>
    /// <param name="background">Indicates if the update generator thread should run as a background thread</param>
    public void Start(bool background = true)
    {
      m_background = background;
      if (m_updateThread != null)
      {
        m_updateThread.Interrupt();
      }

      m_updateThread = new Thread(UpdateGenerator)
      {
        IsBackground = background
      };

      m_running = true;
      m_updateThread.Start();
    }

    /// <summary>
    /// Sets the running flag to false, causing the update thread to stop
    /// </summary>
    public void Stop()
    {
      m_running = false;
    }

    /// <summary>
    /// Blocks waiting for the update thread to end
    /// </summary>
    public void Join()
    {
      m_updateThread?.Join();
    }

    /// <summary>
    /// Blocks waiting to join the update thread up to a max timeout
    /// </summary>
    /// <param name="milliseconds">The max time in milliseconds to wait for the update thread to end</param>
    /// <returns>True if the update thread ended, false if a timeout occurred</returns>
    public bool Join(int milliseconds)
    {
      return m_updateThread?.Join(milliseconds) ?? true;
    }

    /// <summary>
    /// An event generating loop that periodically calls subscribed update callbacks.
    /// </summary>
    private void UpdateGenerator()
    {
      Stopwatch stopwatch = Stopwatch.StartNew();
      double deltaTime = 0;
      if (m_maxUpdateHz <= 0)
      {
        m_maxUpdateHz = 1;
      }

      double minPeriod = 1 / m_maxUpdateHz;
      m_updateTime = new UpdateTime();
      stopwatch.Start();

      // Track how much the sleep time drifts from our actual expected sleep duration
      // so we can compensate for that time
      double sleepDrift = 0;

      while (m_running)
      {
        OnUpdate?.Invoke(m_updateTime.Copy());

        deltaTime = stopwatch.Elapsed.TotalSeconds - m_updateTime.TotalSeconds;
        if (sleepDrift < minPeriod)
        {
          if (deltaTime < minPeriod)
          {
            // Calculate 100ns ticks to wait
            double sleepSecs = minPeriod - deltaTime;
            long ticks = (long)(sleepSecs / 100e-9);
            Thread.Sleep(new TimeSpan(ticks));
            double oldDelta = deltaTime;
            deltaTime = stopwatch.Elapsed.TotalSeconds - m_updateTime.TotalSeconds;
            sleepDrift += deltaTime - (oldDelta + sleepSecs);

            // Check if we're drifting faster than the max rate
            while (sleepDrift < minPeriod * -1)
            {
              // We've slept too little
              sleepSecs = -1 * sleepDrift;
              ticks = (long)(sleepSecs / 100e-9);
              Thread.Sleep(new TimeSpan(ticks));
              sleepSecs = (stopwatch.Elapsed.TotalSeconds - m_updateTime.TotalSeconds) - deltaTime;
              sleepDrift += sleepSecs;
              deltaTime += sleepSecs;
            }
          }
        }
        else
        {
          sleepDrift -= (minPeriod + deltaTime);
        }

        m_updateTime.TotalSeconds = stopwatch.Elapsed.TotalSeconds;
        m_updateTime.DeltaSeconds = deltaTime;
      }

      stopwatch?.Stop();
    }

    /// <summary>
    /// Sets the max update frequency for this update generator. Stops and restarts the update thread if it is currently running.
    /// </summary>
    /// <param name="maxUpdateHz">The new max update frequency to set</param>
    private void SetMaxUpdate(double maxUpdateHz)
    {
      m_maxUpdateHz = maxUpdateHz;
      if (m_running)
      {
        Stop();
        Join();
        Start(m_background);
      }
    }
  }
}
