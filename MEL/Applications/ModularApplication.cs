﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;

namespace Skulk.MEL
{
  /// <summary>
  /// A discrete event callback.
  /// </summary>
  /// <param name="eventId">The ID associated with the event.</param>
  public delegate void DiscreteEventCallback(int eventId);

  /// <summary>
  /// An application class that is designed to provide functionality through the use of application modules.
  /// </summary>
  /// <remarks>
  /// This application derives from PeriodicUpdateApplication and thus has both run and update threads.
  /// </remarks>
  public class ModularApplication : PeriodicUpdateApplication, IModularObject
  {
    private ModuleManager m_moduleManager;

    /// <summary>
    /// Gets or sets the logger module of this application, uses the static log if no log module is attached.
    /// </summary>
    public ILogger Log { get => m_moduleManager?.Log; set => m_moduleManager.Log = value; }

    /// <summary>
    /// Initializes a new instance of the <see cref="GenericModularApplication{ModuleParentType}"/> class.
    /// </summary>
    /// <param name="log">The log instance to use on this application.</param>
    public ModularApplication(ILogger log = null)
    {
      m_moduleManager = new ModuleManager(log);
      OnUpdate += m_moduleManager.Update;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ModularApplication"/> class with the specified update rate.
    /// </summary>
    /// <param name="maxUpdateHz">The maximum rate that update events will be generated.</param>
    /// <param name="log">The log instance to use on this application.</param>
    /// <param name="updateGenerator">The class instance to use for generating periodic updates, if null a <see cref="PeriodicUpdateGenerator"/> instance will be used.</param>
    public ModularApplication(double maxUpdateHz, ILogger log = null, IUpdateGenerator updateGenerator = null) : base(maxUpdateHz, updateGenerator)
    {
      m_moduleManager = new ModuleManager(log);
      OnUpdate += m_moduleManager.Update;
    }

    /// <summary>
    /// Starts the application and executes module startups
    /// </summary>
    /// <param name="background">Indicates whether the run thread for this application should be a background thread</param>
    public override void Start(bool background = false)
    {
      m_moduleManager.StartUp();
      base.Start(background);
    }

    /// <summary>
    /// Stops the application and all module threads.
    /// </summary>
    public override void Stop()
    {
      base.Stop();
      m_moduleManager.ShutDown();
    }

    /// <summary>
    /// Adds a module to this application.
    /// </summary>
    /// <param name="module">The module to add to this application.</param>
    /// <returns>True if the module was added, false otherwise.</returns>
    public virtual bool AddModule(IModule module)
    {
      return m_moduleManager.AddModule(module);
    }

    /// <summary>
    /// Removes a module of the specified type from this application.
    /// </summary>
    /// <param name="moduleType">The type of the module to remove</param>
    /// <returns>True if a module of the specified type was removed, false otherwise</returns>
    public virtual bool RemoveModule(Type moduleType)
    {
      return m_moduleManager.RemoveModule(moduleType);
    }

    /// <summary>
    /// Removes a module of the specified type from this application.
    /// </summary>
    /// <typeparam name="T">The type of the module to remove</typeparam>
    /// <returns>True if a module of the specified type was removed, false otherwise</returns>
    public virtual bool RemoveModule<T>()
    {
      return m_moduleManager.RemoveModule<T>();
    }

    /// <summary>
    /// Removes a module of the specified type name from this application.
    /// </summary>
    /// <param name="moduleTypeName">The type name of the module to remove</param>
    /// <returns>True if a module of the specified type was removed, false otherwise</returns>
    public virtual bool RemoveModule(string moduleTypeName)
    {
      return m_moduleManager.RemoveModule(moduleTypeName);
    }

    /// <summary>
    /// Removes the specified module instance from this application
    /// </summary>
    /// <param name="module">The module instance to remove</param>
    /// <returns>True if the module was removed, false otherwise</returns>
    public virtual bool RemoveModule(IModule module)
    {
      return m_moduleManager.RemoveModule(module);
    }

    /// <summary>
    /// Attempts to get the module of the specified type on this application.
    /// </summary>
    /// <typeparam name="T">The type of the module to get.</typeparam>
    /// <returns>The module of the specified type or null if none exists.</returns>
    public virtual T GetModule<T>() where T : IModule
    {
      return m_moduleManager.GetModule<T>();
    }

    /// <summary>
    /// Attempts to get the module with the specified type on this application.
    /// </summary>
    /// <param name="moduleType">The type of the module to get</param>
    /// <returns>The module of the specified type or null if none exists</returns>
    public virtual IModule GetModule(Type moduleType)
    {
      return m_moduleManager.GetModule(moduleType);
    }

    /// <summary>
    /// Attempts to get a module with the specified type name.
    /// </summary>
    /// <param name="moduleTypeName">The name of the module type to get</param>
    /// <returns>The module of the specified type or null if no matching module exists</returns>
    public virtual IModule GetModule(string moduleTypeName)
    {
      return m_moduleManager.GetModule(moduleTypeName);
    }

    /// <summary>
    /// Method called when a discrete event starts.
    /// </summary>
    /// <param name="eventId">The event that is starting.</param>
    public virtual void EventStart(int eventId)
    {
      m_moduleManager.EventStart(eventId);
    }

    /// <summary>
    /// Method called for when a discrete event is ending.
    /// </summary>
    /// <param name="eventId">The event that is ending.</param>
    public virtual void EventEnd(int eventId)
    {
      m_moduleManager.EventEnd(eventId);
    }

    /// <summary>
    /// Gets all the modules on this application in an enumerable list.
    /// </summary>
    /// <returns>An enumerable list of all the modules held by this application.</returns>
    public virtual IEnumerable<IModule> GetModules()
    {
      return m_moduleManager.GetModules();
    }

    /// <inheritdoc/>
    public virtual List<IModule> GetModules(Type type)
    {
      return m_moduleManager.GetModules(type);
    }
  }
}
