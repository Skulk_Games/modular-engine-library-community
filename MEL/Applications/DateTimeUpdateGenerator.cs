﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2020  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Skulk.MEL
{
  /// <summary>
  /// A class that generates periodic updates utilizing the <see cref="DateTime"/> object.
  /// </summary>
  /// <remarks>
  /// This generator is less precise than the <see cref="PeriodicUpdateGenerator"/> but does not
  /// depend on System.Diagnostics making this more portable.
  /// </remarks>
  public class DateTimeUpdateGenerator : IUpdateGenerator
  {
    private volatile bool m_running = false;
    private bool m_background = false;
    private double m_maxUpdateHz = 60;
    private Thread m_updateThread;
    UpdateTime m_updateTime;

    /// <summary>
    /// A value indicating if the update generating thread is running
    /// </summary>
    public bool Running { get => m_running; }

    /// <summary>
    /// The max rate at which the update event can be generated
    /// </summary>
    public double MaxUpdateHz { get => m_maxUpdateHz; set => SetMaxUpdate(value); }

    /// <summary>
    /// The periodically generated update event
    /// </summary>
    public event UpdateCallback OnUpdate;

    /// <summary>
    /// Default constructor
    /// </summary>
    public DateTimeUpdateGenerator()
    {
      // Empty
    }

    /// <summary>
    /// Initializes an instance of this update generator at the specified max frequency
    /// </summary>
    /// <param name="maxUpdateHz">The max frequency that update events should be generated</param>
    public DateTimeUpdateGenerator(double maxUpdateHz)
    {
      m_maxUpdateHz = maxUpdateHz;
    }

    /// <summary>
    /// Blocks the calling thread until the update thread stops
    /// </summary>
    public void Join()
    {
      m_updateThread?.Join();
    }

    /// <summary>
    /// Blocks the calling thread until the update thread stops or the specified time elapses
    /// </summary>
    /// <param name="milliseconds">The max time to wait</param>
    /// <returns>True if the thread is not running, false if the thread is still running</returns>
    public bool Join(int milliseconds)
    {
      return m_updateThread?.Join(milliseconds) ?? true;
    }

    /// <summary>
    /// Starts the update generator
    /// </summary>
    /// <param name="background">Indicates if the update generator thread should run as a background thread</param>
    public void Start(bool background = true)
    {
      m_background = background;
      Stop();
      m_updateThread?.Join();

      m_updateThread = new Thread(UpdateLoop)
      {
        IsBackground = background
      };
      m_running = true;
      m_updateThread.Start();
    }

    /// <summary>
    /// Sets the running flag to false, causing the update thread to stop
    /// </summary>
    public void Stop()
    {
      m_running = false;
    }

    /// <summary>
    /// The update generating loop
    /// </summary>
    private void UpdateLoop()
    {
      DateTime time = DateTime.UtcNow;
      TimeSpan deltaSpan = new TimeSpan();

      if (m_maxUpdateHz <= 0)
      {
        m_maxUpdateHz = 1;
      }

      double minPeriod = 1 / m_maxUpdateHz;
      TimeSpan minSpan = TimeSpan.FromSeconds(minPeriod);
      m_updateTime = new UpdateTime();
      DateTime startTime = DateTime.UtcNow;

      // Track the sleep drift so we can compensate for the desired vs actual sleep time
      TimeSpan sleepDrift = new TimeSpan();

      while (m_running)
      {
        OnUpdate?.Invoke(m_updateTime.Copy());

        deltaSpan = (DateTime.UtcNow - time);
        if (sleepDrift < minSpan)
        {
          if (deltaSpan < minSpan)
          {
            TimeSpan sleepSpan = (minSpan - deltaSpan);
            Thread.Sleep(sleepSpan);
            TimeSpan oldSpan = deltaSpan;
            deltaSpan = (DateTime.UtcNow - time);
            sleepDrift += deltaSpan - (oldSpan + sleepSpan);

            // Check if we're drifting faster than the max rate
            while (sleepDrift.TotalSeconds < minSpan.TotalSeconds * -1)
            {
              // We've slept too little
              Thread.Sleep(new TimeSpan(-1 * sleepDrift.Ticks));
              sleepSpan = (DateTime.UtcNow - time) - deltaSpan;
              sleepDrift += sleepSpan;
              deltaSpan += sleepSpan;
            }
          }
        }
        else
        {
          sleepDrift -= (minSpan + deltaSpan);
        }

        time = DateTime.UtcNow;
        m_updateTime.DeltaSeconds = deltaSpan.TotalSeconds;
        m_updateTime.TotalSeconds = (time - startTime).TotalSeconds;
      }
    }

    /// <summary>
    /// Sets the max update frequency and restarts the update generator if it is currently running.
    /// </summary>
    /// <param name="maxUpdateHz">The new max frequency value to set</param>
    private void SetMaxUpdate(double maxUpdateHz)
    {
      m_maxUpdateHz = maxUpdateHz;
      if (m_running)
      {
        Stop();
        Join();
        Start(m_background);
      }
    }
  }
}
