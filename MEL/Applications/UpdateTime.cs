﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2020  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Text;

namespace Skulk.MEL
{
  /// <summary>
  /// A class holding timing information used in periodic updates.
  /// </summary>
  public class UpdateTime
  {
    private double m_totalSeconds;
    private double m_deltaSeconds;

    /// <summary>
    /// The total time in seconds that the application update thread has been running
    /// </summary>
    public double TotalSeconds { get => m_totalSeconds; set => m_totalSeconds = value; }

    /// <summary>
    /// The time in seconds since the last update
    /// </summary>
    public double DeltaSeconds { get => m_deltaSeconds; set => m_deltaSeconds = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public UpdateTime()
    {
      // Empty
    }

    /// <summary>
    /// Initializes a new instance of <see cref="UpdateTime"/> with the specified values.
    /// </summary>
    /// <param name="totalSeconds">The total elapsed seconds of the application update thread.</param>
    /// <param name="deltaSeconds">The time in seconds since the last time update was invoked.</param>
    public UpdateTime(double totalSeconds, double deltaSeconds)
    {
      m_totalSeconds = totalSeconds;
      m_deltaSeconds = deltaSeconds;
    }

    /// <summary>
    /// Creates a new instances of the <see cref="UpdateTime"/> class with values copied from this instance.
    /// </summary>
    /// <returns>The new instance with copied values.</returns>
    public UpdateTime Copy()
    {
      return new UpdateTime(m_totalSeconds, m_deltaSeconds);
    }
  }
}
