﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Skulk.MEL
{
  /// <summary>
  /// The log message level flags
  /// </summary>
  public enum LogLevels : byte
  {
    /// <summary>
    /// The bit that enables error log messages
    /// </summary>
    ERROR = 0x01,

    /// <summary>
    /// The bit that enables warning log messages
    /// </summary>
    WARNING = 0x02,

    /// <summary>
    /// The bit that enables info log messages
    /// </summary>
    INFO = 0x04,

    /// <summary>
    /// The bit that enables debug log messages
    /// </summary>
    DEBUG = 0x08,

    /// <summary>
    /// The bit that enables trace log messages
    /// </summary>
    TRACE = 0x10,

    /// <summary>
    /// The bitmask that enables all log levels
    /// </summary>
    ALL = 0xFF
  }

  /// <summary>
  /// A class used for logging messages to a file
  /// </summary>
  public class Logger : ThreadedApplication, ILogger
  {
    private const int MAX_LOGS_PER_PERIOD = 1000;
    private const int PERIOD_SLEEP_TIME_MS = 50;

    /// <summary>
    /// The file where log messages will be written
    /// </summary>
    private string m_logFile = "app.log";

    /// <summary>
    /// The bitfield indicating which log levels are enabled.
    /// </summary>
    private byte m_logLevels = 0;

    /// <summary>
    /// The queue where new messages are added waiting to be written to file.
    /// </summary>
    private Queue<string> m_logQueue = new Queue<string>();

    /// <summary>
    /// A flag that indicates if log messages should also be written to the console.
    /// </summary>
    private bool m_enableConsole = false;

    /// <summary>
    /// The stream writer that writes to the log file when open.
    /// </summary>
    StreamWriter m_fout;

    /// <summary>
    /// An object used to lock access to the log stream.
    /// </summary>
    object m_streamLock = new object();

    /// <summary>
    /// Gets or sets the file where log messages will be written
    /// </summary>
    public string LogFile { get => m_logFile; set => m_logFile = value; }

    /// <summary>
    /// Gets or sets the log level bitmask
    /// </summary>
    public byte LogLevel { get => m_logLevels; set => m_logLevels = value; }

    /// <summary>
    /// Gets or sets whether the console log output is enabled
    /// </summary>
    public bool EnableConsole { get => m_enableConsole; set => m_enableConsole = value; }

    /// <inheritdoc/>
    public LoggerStopMode StopMode { get; set; } = LoggerStopMode.Timed;

    /// <inheritdoc/>
    public double StopTime { get; set; } = 1;

    /// <summary>
    /// Initializes a new instance of the <see cref="Logger"/> class with default values.
    /// </summary>
    public Logger()
    {
      // Empty
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Logger"/> class with the indicated values.
    /// </summary>
    /// <param name="logFile">The file where messages will be logged.</param>
    /// <param name="logLevels">The bitmask indicating which levels are enabled.</param>
    public Logger(string logFile, byte logLevels)
    {
      SetLogFile(logFile);
      SetLogLevels(logLevels);
    }

    /// <summary>
    /// Sets the static log file where messages will be written to.
    /// </summary>
    /// <param name="logFile">The file where messages will be logged.</param>
    public void SetLogFile(string logFile)
    {
      m_logFile = logFile;

      // Close the log file to make sure the old file is released
      lock (m_streamLock)
      {
        CloseLog();
      }
    }

    /// <summary>
    /// Sets the enabled log levels
    /// </summary>
    /// <param name="levelMask">The bits indicating which levels to enable.</param>
    public void SetLogLevels(byte levelMask)
    {
      m_logLevels = levelMask;
    }

    /// <summary>
    /// Enables or disables the console log output.
    /// </summary>
    /// <param name="enable">True enables the console log, false disables the console log.</param>
    public void EnableConsoleOutput(bool enable)
    {
      m_enableConsole = enable;
    }

    /// <summary>
    /// Logs the message as an error.
    /// </summary>
    /// <param name="msgFormat">The log message format.</param>
    /// <param name="args">The arguments to place into any message arguments.</param>
    public void LogError(string msgFormat, params object[] args)
    {
      Log(LogLevels.ERROR, msgFormat, args);
    }

    /// <summary>
    /// Logs the message as a warning.
    /// </summary>
    /// <param name="msgFormat">The format of the warning message.</param>
    /// <param name="args">The arguments to place in the formatted message.</param>
    public void LogWarning(string msgFormat, params object[] args)
    {
      Log(LogLevels.WARNING, msgFormat, args);
    }

    /// <summary>
    /// Logs the message as an information message.
    /// </summary>
    /// <param name="msgFormat">The format of the information message.</param>
    /// <param name="args">The arguments to place in the formatted message.</param>
    public void LogInfo(string msgFormat, params object[] args)
    {
      Log(LogLevels.INFO, msgFormat, args);
    }

    /// <summary>
    /// Logs the message as a debug message.
    /// </summary>
    /// <param name="msgFormat">The format of the debug message.</param>
    /// <param name="args">The arguments to place in the formatted message.</param>
    public void LogDebug(string msgFormat, params object[] args)
    {
      Log(LogLevels.DEBUG, msgFormat, args);
    }

    /// <summary>
    /// Logs the message as a trace message.
    /// </summary>
    /// <param name="msgFormat">The format of the trace message.</param>
    /// <param name="args">The arguments to place in the formatted message.</param>
    public void LogTrace(string msgFormat, params object[] args)
    {
      Log(LogLevels.TRACE, msgFormat, args);
    }

    /// <summary>
    /// The logger run loop that processes logged messages and writes them out to file.
    /// </summary>
    protected override void Run()
    {
      while (!StopRun)
      {
        WriteLogs();
        Thread.Sleep(PERIOD_SLEEP_TIME_MS);
      }

      // Handle stop mode logic
      if (StopMode != LoggerStopMode.Immediate)
      {
        switch (StopMode)
        {
          case LoggerStopMode.Patient:
          {
            while (WriteLogs()) ;
            break;
          }

          case LoggerStopMode.Timed:
          {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            while (sw.Elapsed.TotalSeconds < StopTime && WriteLogs()) ;
            sw.Stop();
            break;
          }
        }
      }

      CloseLog();
    }

    /// <summary>
    /// Writes out a batch of any pending logs
    /// </summary>
    /// <returns>True if any logs were written, otherwise false</returns>
    private bool WriteLogs()
    {
      try
      {
        if (m_logQueue.Count > 0)
        {
          lock (m_streamLock)
          {
            // Open file
            if (m_fout == null)
            {
              OpenLog(m_logFile);
            }

            if (m_fout != null)
            {
              // write all logs
              lock (m_logQueue)
              {
                int count = 0;
                while (m_logQueue.Count > 0 && count++ < MAX_LOGS_PER_PERIOD)
                {
                  string msg = m_logQueue.Dequeue();
                  m_fout.WriteLine(msg);
                  if (m_enableConsole)
                  {
                    Console.WriteLine(msg);
                  }
                }
              }

              // Flush
              m_fout.Flush();
            }
          }

          return true;
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine("Exception writing logs: {0}", ex.Message);
        Console.WriteLine(ex.ToString());
      }

      return false;
    }

    /// <summary>
    /// Opens the log file for appending and sets the member streamwriter to output to the file.
    /// </summary>
    /// <param name="filename">The full filename of the log file.</param>
    private void OpenLog(string filename)
    {
      try
      {
        if (m_fout != null)
        {
          m_fout.Close();
        }

        m_fout = new StreamWriter(File.Open(filename, FileMode.Append, FileAccess.Write, FileShare.ReadWrite));
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
      }
    }

    /// <summary>
    /// Closes the member streamwriter that is writing to the log file.
    /// </summary>
    private void CloseLog()
    {
      try
      {
        if (m_fout != null)
        {
          m_fout.Close();
          m_fout = null;
        }
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
      }
    }

    /// <summary>
    /// Logs the specified message at the specified level to the file indicated.
    /// </summary>
    /// <param name="level">The level at which the message should be logged.</param>
    /// <param name="msgFormat">The string format for the message.</param>
    /// <param name="args">Any parameters to fill in to the message.</param>
    protected virtual void Log(LogLevels level, string msgFormat, params object[] args)
    {
      if (IsLevelEnabled(level))
      {
        string line = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.ffff [") + level.ToString() + "]: " + string.Format(msgFormat, args);
        lock (m_logQueue)
        {
          m_logQueue.Enqueue(line);
        }
      }
    }

    /// <summary>
    /// Determines if the indicated level is enabled.
    /// </summary>
    /// <param name="level">The level to check</param>
    /// <returns>True if the level is enabled, false otherwise.</returns>
    private bool IsLevelEnabled(LogLevels level)
    {
      return (m_logLevels & (byte)level) != 0;
    }
  }
}
