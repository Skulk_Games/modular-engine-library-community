﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Text;

namespace Skulk.MEL
{
  /// <summary>
  /// Enumeration of the modes that can be used when stopping a logger
  /// </summary>
  public enum LoggerStopMode
  {
    /// <summary>
    /// Be patient and wait for any pending logs to finish being written
    /// </summary>
    Patient,

    /// <summary>
    /// Wait for any pending logs to finish up to a max time
    /// </summary>
    Timed,

    /// <summary>
    /// Stop immediately potentially dropping pending log messages
    /// </summary>
    Immediate
  }

  /// <summary>
  /// The standard logger interface
  /// </summary>
  public interface ILogger
  {
    /// <summary>
    /// Gets or sets the log file where log messages will be written
    /// </summary>
    string LogFile { get; set; }

    /// <summary>
    /// Gets or sets the enabled log level bitmask
    /// </summary>
    byte LogLevel { get; set; }

    /// <summary>
    /// Gets or sets whether the console log output is enabled
    /// </summary>
    bool EnableConsole { get; set; }

    /// <summary>
    /// Gets or sets the mode the logger should use when stopping
    /// </summary>
    LoggerStopMode StopMode { get; set; }

    /// <summary>
    /// Gets or sets the max time the logger is allowed to write logs
    /// after stop has been called when using <see cref="LoggerStopMode.Timed"/>
    /// </summary>
    double StopTime { get; set; }

    /// <summary>
    /// Sets the file where log messages will be written.
    /// </summary>
    /// <param name="logFile">The full log file path.</param>
    void SetLogFile(string logFile);

    /// <summary>
    /// Sets the enabled log levels.
    /// </summary>
    /// <param name="logLevels">The bitmask of enabled log levels.</param>
    void SetLogLevels(byte logLevels);

    /// <summary>
    /// Enables or disables the console log output.
    /// </summary>
    /// <param name="enable">True enables the console log, false disables it.</param>
    void EnableConsoleOutput(bool enable);

    /// <summary>
    /// Logs an error message.
    /// </summary>
    /// <param name="msgFormat">The error message format.</param>
    /// <param name="args">The argument values for the formatted message.</param>
    void LogError(string msgFormat, params object[] args);

    /// <summary>
    /// Logs a warning message.
    /// </summary>
    /// <param name="msgFormat">The warning message format.</param>
    /// <param name="args">The argument values for the formatted message.</param>
    void LogWarning(string msgFormat, params object[] args);

    /// <summary>
    /// Logs an info message.
    /// </summary>
    /// <param name="msgFormat">The info message format.</param>
    /// <param name="args">The argument values for the formatted message.</param>
    void LogInfo(string msgFormat, params object[] args);

    /// <summary>
    /// Logs a debug message.
    /// </summary>
    /// <param name="msgFormat">The debug message format.</param>
    /// <param name="args">The argument values for the formatted message.</param>
    void LogDebug(string msgFormat, params object[] args);

    /// <summary>
    /// Logs a trace message.
    /// </summary>
    /// <param name="msgFormat">The trace message format.</param>
    /// <param name="args">The argument values for the formatted message.</param>
    void LogTrace(string msgFormat, params object[] args);
  }
}
