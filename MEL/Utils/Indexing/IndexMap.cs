﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Text;

namespace Skulk.MEL.Utils
{
  /// <summary>
  /// Stores information about an indexed collection
  /// </summary>
  /// <typeparam name="T">The class type of the object in an indexed collection</typeparam>
  public class IndexMap<T>
  {
    private UInt32[] m_indices;
    private T[] m_values;

    /// <summary>
    /// The array of index key values
    /// </summary>
    public uint[] Indices { get => m_indices; set => m_indices = value; }

    /// <summary>
    /// The array of index values
    /// </summary>
    public T[] Values { get => m_values; set => m_values = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public IndexMap()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this map with the specified values
    /// </summary>
    /// <param name="indices">The array of indexed value keys</param>
    /// <param name="values">The array of indexed values</param>
    /// <exception cref="Exception">The arrays must be equal lengths</exception>
    public IndexMap(UInt32[] indices, T[] values)
    {
      if (indices != null && values != null)
      {
        if (indices.Length == values.Length)
        {
          m_indices = indices;
          m_values = values;
        }
        else
        {
          throw new Exception("InvalidArrayLength: The index and value array lengths do not match");
        }
      }
    }
  }
}
