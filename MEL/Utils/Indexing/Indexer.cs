﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Text;

namespace Skulk.MEL.Utils
{
  /// <summary>
  /// A utility class used to associate objects with an ID value
  /// </summary>
  /// <typeparam name="T">The type of object to index</typeparam>
  public class Indexer<T>
  {
    /// <summary>
    /// Value used to track the next ID in the sequence to use
    /// </summary>
    private UInt32 m_nextId;

    /// <summary>
    /// The dictionary for finding objects by ID
    /// </summary>
    private Dictionary<UInt32, T> m_index;

    /// <summary>
    /// The dictionary for finding IDs by object
    /// </summary>
    private Dictionary<T, UInt32> m_lookup;

    /// <summary>
    /// Default constructor
    /// </summary>
    public Indexer()
    {
      m_index = new Dictionary<uint, T>();
      m_lookup = new Dictionary<T, uint>();
    }

    /// <summary>
    /// Initializes a new <see cref="Indexer{T}"/> instance with the specified initial capacity
    /// </summary>
    /// <param name="capacity">The initial capacity of this indexer</param>
    public Indexer(int capacity)
    {
      m_index = new Dictionary<uint, T>(capacity);
      m_lookup = new Dictionary<T, uint>(capacity);
    }

    /// <summary>
    /// Indexes the specified object and assigns an ID value for it
    /// </summary>
    /// <param name="obj">The object to index</param>
    /// <param name="id">The ID value assigned to the object</param>
    /// <returns>True if the object was able to be indexed, false otherwise</returns>
    /// <remarks>Typically the only reason for a failure is there are too many objects being indexed, there is a limit of UInt32.MaxValue</remarks>
    public bool Index(T obj, out UInt32 id)
    {
      id = 0;
      lock (m_index)
      {
        if (!m_lookup.ContainsKey(obj))
        {
          for (UInt32 i = 0; m_index.ContainsKey(m_nextId) && i < UInt32.MaxValue; ++i)
          {
            ++m_nextId;
          }

          m_index.Add(m_nextId, obj);
          m_lookup.Add(obj, m_nextId);
          id = m_nextId++;
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// Removes an object from the indexing
    /// </summary>
    /// <param name="obj">The object to remove</param>
    /// <returns>True if the object was found and removed, false otherwise</returns>
    public bool Remove(T obj)
    {
      lock (m_index)
      {
        if (m_lookup.TryGetValue(obj, out UInt32 id))
        {
          m_lookup.Remove(obj);
          m_index.Remove(id);
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// Removes an object from the indexing
    /// </summary>
    /// <param name="id">The ID of the object to remove</param>
    /// <returns>True if the object was found and removed, false otherwise</returns>
    public bool Remove(UInt32 id)
    {
      lock (m_index)
      {
        if (m_index.TryGetValue(id, out T obj))
        {
          m_index.Remove(id);
          m_lookup.Remove(obj);
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// Gets the ID value assigned to an object
    /// </summary>
    /// <param name="obj">The object to get the ID of</param>
    /// <param name="id">The ID of the specified object</param>
    /// <returns>True if the object was found, false otherwise</returns>
    public bool GetIndex(T obj, out UInt32 id)
    {
      lock (m_index)
      {
        return m_lookup.TryGetValue(obj, out id);
      }
    }

    /// <summary>
    /// Gets the object with the specified ID
    /// </summary>
    /// <param name="id">The ID of the indexed object to get</param>
    /// <param name="obj">The object that matched the specified ID</param>
    /// <returns>True if the object was found, false otherwise</returns>
    public bool GetObject(UInt32 id, out T obj)
    {
      lock (m_index)
      {
        return m_index.TryGetValue(id, out obj);
      }
    }

    /// <summary>
    /// Gets a newly created index map for the values in this indexer
    /// </summary>
    /// <returns>The newly created index map referencing the values in this indexer</returns>
    public IndexMap<T> GetMap()
    {
      lock (m_index)
      {
        UInt32[] keys = new uint[m_index.Count];
        m_index.Keys.CopyTo(keys, 0);
        T[] values = new T[m_index.Count];
        m_index.Values.CopyTo(values, 0);
        return new IndexMap<T>(keys, values);
      }
    }
  }
}
