﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skulk.MEL.Utils
{
  /// <summary>
  /// Contains utility methods for parsing strings
  /// </summary>
  public static class ParseUtils
  {
    /// <summary>
    /// Splits a string with the specified delimiter but ignores any delimiters within quotes
    /// </summary>
    /// <param name="valStr">The string to split</param>
    /// <param name="delimiter">The character to split on</param>
    /// <returns>The array of string values after splitting</returns>
    public static string[] SplitOutsideQuotes(string valStr, char delimiter)
    {
      StringBuilder sb = new StringBuilder();
      if (valStr != null)
      {
        List<string> result = new List<string>();
        bool inQuotes = false;
        for (int i = 0; i < valStr.Length; i++)
        {
          switch(valStr[i])
          {
            case '"':
            {
              inQuotes = !inQuotes;
              sb.Append('"');
            }
            break;

            case '\'':
            {
              inQuotes = !inQuotes;
              sb.Append('\'');
            }
            break;

            default:
            {
              if (!inQuotes && valStr[i] == delimiter)
              {
                result.Add(sb.ToString());
                sb = new StringBuilder();
              }
              else
              {
                sb.Append(valStr[i]);
              }
            }
            break;
          }
        }

        result.Add(sb.ToString());
        return result.ToArray();
      }

      return null;
    }

    /// <summary>
    /// Gets the first string between the specified open and close string
    /// </summary>
    /// <param name="text">The text to get the substring from</param>
    /// <param name="openText">The string preceeding the text to get</param>
    /// <param name="closeText">The string following the text to get</param>
    /// <returns>The substring between the open and close text or null if no text was found</returns>
    public static string GetInnerText(string text, string openText, string closeText)
    {
      int startIndex = text.IndexOf(openText);
      if (startIndex >= 0)
      {
        int endIndex = text.IndexOf(closeText, startIndex + openText.Length);
        if (endIndex > startIndex)
        {
          return text.Substring(startIndex + openText.Length, endIndex - (startIndex + openText.Length));
        }
      }

      return null;
    }
  }
}
