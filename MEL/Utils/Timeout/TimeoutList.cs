﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;

namespace Skulk.MEL.Utils
{
  /// <summary>
  /// A callback for when an item in the timeout list expires.
  /// </summary>
  /// <typeparam name="T">The type of the item in the list.</typeparam>
  /// <param name="item">The item that expired and was removed from the list.</param>
  public delegate void TimeoutItemExpired<T>(T item);

  /// <summary>
  /// Holds a list of items, but only for a specified amount of time.
  /// </summary>
  /// 
  /// <remarks>
  /// This structure is designed to work within a module that handles period Update calls.
  /// </remarks>
  /// 
  /// <typeparam name="T">The type of the objects to hold.</typeparam>
  public class TimeoutList<T> : IRealtimeUpdate
  {
    private const double DEFAULT_TIMEOUT = 10;
    private List<TimeoutItem<T>> m_items = new List<TimeoutItem<T>>();

    /// <summary>
    /// A callback invoked when an item expires and is removed from the list.
    /// </summary>
    public event TimeoutItemExpired<T> OnItemExpired;

    /// <summary>
    /// The number of items currently in this list
    /// </summary>
    public int Count { get => GetCount(); }

    /// <summary>
    /// Initializes a new <see cref="TimeoutList{T}"/> with default initial capacity.
    /// </summary>
    public TimeoutList()
    {
      // Empty
    }

    /// <summary>
    /// Initializes a new <see cref="TimeoutList{T}"/> with the specified initial capacity
    /// </summary>
    /// <param name="capacity">The initial capacity of the list</param>
    public TimeoutList(int capacity)
    {
      m_items = new List<TimeoutItem<T>>(capacity);
    }

    /// <summary>
    /// Gets or sets the item at the specified index. Care should be taken with this call as the list is not locked
    /// and any call to Update could remove items from the list.
    /// </summary>
    /// <param name="i">The index of the item.</param>
    /// <returns>The item.</returns>
    public T this[int i]
    {
      get { return Get(i); }
      set { Set(i, value); }
    }

    /// <summary>
    /// Adds the item to the list for the specified max time.
    /// </summary>
    /// <param name="item">The item to add.</param>
    /// <param name="timeout">The max time in seconds the item will stay in the list.</param>
    public virtual void Add(T item, double timeout = DEFAULT_TIMEOUT)
    {
      lock (m_items)
      {
        m_items.Add(new TimeoutItem<T>(item, timeout));
      }
    }

    /// <summary>
    /// Removes the item from the list.
    /// </summary>
    /// <param name="item">The item to remove.</param>
    /// <returns>True if the item was found and removed, otherwise false.</returns>
    public virtual bool Remove(T item)
    {
      lock (m_items)
      {
        TimeoutItem<T> tItem = m_items.Find(x => x.Item.Equals(item));
        if (tItem != null)
        {
          return m_items.Remove(tItem);
        }
      }

      return false;
    }

    /// <summary>
    /// Determines whether the specified item is in the list.
    /// </summary>
    /// <param name="item">The item to check for.</param>
    /// <returns>True if the item is in the list, otherwise false.</returns>
    public virtual bool Contains(T item)
    {
      lock (m_items)
      {
        return m_items.Find(x => x.Item.Equals(item)) != null;
      }
    }

    /// <summary>
    /// Searches for an element that matches the condition specified and returns the first item found
    /// </summary>
    /// <param name="match">The predicate that defines the matching condition</param>
    /// <returns>The item found or the default value for the class if no matching item was found.</returns>
    public virtual T Find(Predicate<T> match)
    {
      TimeoutItem<T> tItem;
      lock (m_items)
      {
         tItem = m_items.Find(x => match.Invoke(x.Item));
      }

      if (tItem != null)
      {
        return tItem.Item;
      }

      return default(T);
    }

    /// <summary>
    /// Finds the index of the first item matching the conditions specified and returns the index of the item
    /// </summary>
    /// <param name="match">The predicate that defines the matching condition</param>
    /// <returns>The index of the first item found matching the condition</returns>
    public virtual int FindIndex(Predicate<T> match)
    {
      lock (m_items)
      {
        return m_items.FindIndex(x => match.Invoke(x.Item));
      }
    }

    /// <summary>
    /// Removes the item in the list at the specified index
    /// </summary>
    /// <param name="index">The index of the item to remove.</param>
    public virtual void RemoveAt(int index)
    {
      lock (m_items)
      {
        m_items.RemoveAt(index);
      }
    }

    /// <summary>
    /// Resets the elapsed time in the list for the item at the specified index
    /// </summary>
    /// <param name="index">The index of the item to reset elapsed time on</param>
    public virtual void Reset(int index)
    {
      lock (m_items)
      {
        if (index >= 0 && index < m_items.Count)
        {
          m_items[index].Time = 0;
        }
      }
    }

    /// <summary>
    /// Updates the times for each item and removes expired items from the list.
    /// This should be called periodically.
    /// </summary>
    /// <param name="time">The object containing update timing information.</param>
    public virtual void Update(UpdateTime time)
    {
      List<T> expiredItems = new List<T>();
      lock (m_items)
      {
        for (int i = m_items.Count - 1; i >= 0; --i)
        {
          m_items[i].Update(time.DeltaSeconds);
          if (m_items[i].Expired)
          {
            expiredItems.Add(m_items[i].Item);
            m_items.RemoveAt(i);
          }
        }
      }

      for (int i = 0; i < expiredItems.Count; ++i)
      {
        OnItemExpired?.Invoke(expiredItems[i]);
      }
    }

    /// <summary>
    /// Locks the list and returns an array containing all the items currently in the list
    /// </summary>
    /// <returns>An array containing all the items in the list</returns>
    public virtual T[] ToArray()
    {
      lock (m_items)
      {
        T[] items = new T[m_items.Count];
        for (int i = 0; i < items.Length; ++i)
        {
          items[i] = m_items[i].Item;
        }

        return items;
      }
    }

    /// <summary>
    /// Gets the number of items in the list
    /// </summary>
    /// <returns>The number of items in the list</returns>
    public int GetCount()
    {
      lock(m_items)
      {
        return m_items.Count;
      }
    }

    /// <summary>
    /// Gets the item at the index specified
    /// </summary>
    /// <param name="index">The index in the list of the item to get</param>
    /// <returns>The item in the list at the specified index or the default value if the index is invalid</returns>
    public T Get(int index)
    {
      lock (m_items)
      {
        if (index >= 0 && index < m_items.Count)
        {
          return m_items[index].Item;
        }
      }
      return default(T);
    }

    /// <summary>
    /// Sets the item at the index specified
    /// </summary>
    /// <param name="index">The index in the list of the item to set</param>
    /// <param name="value">The value of the item to set</param>
    public void Set(int index, T value)
    {
      lock (m_items)
      {
        if (index >= 0 && index < m_items.Count)
        {
          m_items[index].Item = value;
        }
      }
    }
  }
}
