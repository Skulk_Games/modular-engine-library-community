﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;

namespace Skulk.MEL.Utils
{
  /// <summary>
  /// A class that implements a dictionary with values that expire after a specified duration
  /// </summary>
  /// <typeparam name="TKey">The class type of the keys</typeparam>
  /// <typeparam name="TItem">The class type of the item values</typeparam>
  public class TimeoutDictionary<TKey,TItem> : IRealtimeUpdate
  {
    private const double DEFAULT_TIMEOUT = 10;
    private Dictionary<TKey, TimeoutItem<TItem>> m_dictionary = new Dictionary<TKey, TimeoutItem<TItem>>();

    /// <summary>
    /// Event invoked when an item expires from the dictionary
    /// </summary>
    public event TimeoutItemExpired<TItem> OnItemExpired;

    /// <summary>
    /// An array of the keys in this dictionary
    /// </summary>
    public TKey[] Keys { get => GetKeys(); }

    /// <summary>
    /// Gets the array of items in this dictionary
    /// </summary>
    public TItem[] Values { get => GetItems(); }

    /// <summary>
    /// Gets or sets the item with the specified key.
    /// </summary>
    /// <param name="key">The key of the item to access.</param>
    /// <returns>The item.</returns>
    public TItem this[TKey key]
    {
      get { return Get(key); }
      set { Set(key, value); }
    }

    /// <summary>
    /// The number of items in the dictionary
    /// </summary>
    public int Count { get => m_dictionary.Count; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public TimeoutDictionary()
    {
      // Empty
    }

    /// <summary>
    /// Initializes a <see cref="TimeoutDictionary{TKey, TItem}"/> class with the specified initial capacity
    /// </summary>
    /// <param name="capacity">The initial capacity of the dictionary</param>
    public TimeoutDictionary(int capacity)
    {
      m_dictionary = new Dictionary<TKey, TimeoutItem<TItem>>(capacity);
    }

    /// <summary>
    /// Update method that should periodically be called to update the times for items in the dictionary
    /// </summary>
    /// <param name="time">The object containing update timing information</param>
    public void Update(UpdateTime time)
    {
      List<TItem> expiredItems = new List<TItem>();
      List<TKey> expiredKeys = new List<TKey>();
      lock (m_dictionary)
      {
        foreach (KeyValuePair<TKey, TimeoutItem<TItem>> item in m_dictionary)
        {
          item.Value.Update(time.DeltaSeconds);
          if (item.Value.Expired)
          {
            expiredKeys.Add(item.Key);
            expiredItems.Add(item.Value.Item);
          }
        }

        foreach(TKey key in expiredKeys)
        {
          m_dictionary.Remove(key);
        }
      }


      for (int i = 0; i < expiredItems.Count; ++i)
      {
        try
        {
          OnItemExpired?.Invoke(expiredItems[i]);
        }
        catch (Exception ex)
        {
          Console.WriteLine("Exception in {0} method: '{1}'", nameof(OnItemExpired), ex.Message);
        }
      }
    }

    /// <summary>
    /// Adds the key and item value to the dictionary
    /// </summary>
    /// <param name="key">The key of the item to add</param>
    /// <param name="item">The item value to add</param>
    /// <param name="time">The time in seconds for the item to remain in the dictionary</param>
    public void Add(TKey key, TItem item, double time = DEFAULT_TIMEOUT)
    {
      lock (m_dictionary)
      {
        m_dictionary.Add(key, new TimeoutItem<TItem>(item, time));
      }
    }

    /// <summary>
    /// Tries to add the key and item value to the dictionary
    /// </summary>
    /// <param name="key">The key of the item to add</param>
    /// <param name="item">The item value to add</param>
    /// <param name="time">The time in seconds for the item to remain in the dictionary</param>
    /// <returns>True if the item was added, false otherwise (usually indicates the key is already present)</returns>
    public bool TryAdd(TKey key, TItem item, double time = DEFAULT_TIMEOUT)
    {
      lock (m_dictionary)
      {
        if (!m_dictionary.ContainsKey(key))
        {
          m_dictionary.Add(key, new TimeoutItem<TItem>(item, time));
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// Removes the specified key from the dictionary
    /// </summary>
    /// <param name="key">The key to remove</param>
    /// <returns>True if the key was removed, false otherwise</returns>
    public bool Remove(TKey key)
    {
      lock (m_dictionary)
      {
        return m_dictionary.Remove(key);
      }
    }

    /// <summary>
    /// Gets the item that has the specified key
    /// </summary>
    /// <param name="key">The key of the item to get</param>
    /// <returns>The item with the specified key or the default value of TItem if the key does not exist</returns>
    public TItem Get(TKey key)
    {
      lock (m_dictionary)
      {
        if (m_dictionary.TryGetValue(key, out TimeoutItem<TItem> tItem))
        {
          return tItem.Item;
        }
      }

      return default(TItem);
    }

    /// <summary>
    /// Attempts to get the item with the specified key
    /// </summary>
    /// <param name="key">The key of the item to get</param>
    /// <param name="item">The item found with the specified key or default if the key was not found</param>
    /// <returns>True if the item was found, false otherwise</returns>
    public bool TryGetValue(TKey key, out TItem item)
    {
      lock (m_dictionary)
      {
        if (m_dictionary.TryGetValue(key, out TimeoutItem<TItem> tItem))
        {
          item = tItem.Item;
          return true;
        }
      }

      item = default(TItem);
      return false;
    }

    /// <summary>
    /// Sets the item with the specified key, the key must already be in the dictionary
    /// </summary>
    /// <param name="key">The key of the item to set</param>
    /// <param name="item">The item value to set for the key</param>
    /// <returns>True if the item was set, false otherwise</returns>
    public bool Set(TKey key, TItem item)
    {
      lock (m_dictionary)
      {
        if (m_dictionary.TryGetValue(key, out TimeoutItem<TItem> tItem))
        {
          tItem.Item = item;
          return true;
        }
      }

      return false;
    }

    /// <summary>
    /// Resets the elapsed time in the dictionary for the item with the specified key
    /// </summary>
    /// <param name="key">The key of the item to reset elapsed time on</param>
    public virtual void Reset(TKey key)
    {
      lock (m_dictionary)
      {
        if (m_dictionary.TryGetValue(key, out TimeoutItem<TItem> tItem))
        {
          tItem.Time = 0;
        }
      }
    }

    /// <summary>
    /// Constructs a new array with the keys and values copied from the dictionary
    /// </summary>
    /// <returns>The array of key value pairs in the dictionary</returns>
    public KeyValuePair<TKey, TItem>[] ToArray()
    {
      lock (m_dictionary)
      {
        List<KeyValuePair<TKey, TItem>> arr = new List<KeyValuePair<TKey, TItem>>(m_dictionary.Count);
        foreach(var pair in m_dictionary)
        {
          arr.Add(new KeyValuePair<TKey, TItem>(pair.Key, pair.Value.Item));
        }
        return arr.ToArray();
      }
    }

    /// <summary>
    /// Determines whether the specified key is present in the dictionary
    /// </summary>
    /// <param name="key">The key to find</param>
    /// <returns>True if the key is present, false otherwise</returns>
    public bool ContainsKey(TKey key)
    {
      lock (m_dictionary)
      {
        return m_dictionary.ContainsKey(key);
      }
    }

    /// <summary>
    /// Gets the array of key values in this dictionary
    /// </summary>
    /// <returns>An array of key values in this dictionary</returns>
    private TKey[] GetKeys()
    {
      lock (m_dictionary)
      {
        return m_dictionary.Keys.ToArray();
      }
    }

    /// <summary>
    /// Gets the array of items in this dictionary
    /// </summary>
    /// <returns>The array of item objects in this dictionary</returns>
    private TItem[] GetItems()
    {
      lock (m_dictionary)
      {
        TimeoutItem<TItem>[] timeoutItems = m_dictionary.Values.ToArray();
        TItem[] items = new TItem[timeoutItems.Length];
        for (int i = 0; i < timeoutItems.Length; ++i)
        {
          items[i] = timeoutItems[i].Item;
        }
        return items;
      }
    }
  }
}
