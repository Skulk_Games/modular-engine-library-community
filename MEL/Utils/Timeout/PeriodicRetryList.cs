﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skulk.MEL.Utils
{
  /// <summary>
  /// A callback for when an item is removed from a <see cref="PeriodicRetryList{T}"/>.
  /// </summary>
  /// <typeparam name="T">The type of item held in the list.</typeparam>
  /// <param name="item">The item that was removed.</param>
  public delegate void RetryItemRemovedCallback<T>(T item);

  /// <summary>
  /// A generic list that holds an item for a certain amount of time and triggers callbacks when timeouts occur.
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public class PeriodicRetryList<T> : IRealtimeUpdate
  {
    private const int DEFAULT_MAX_RETRY = 3;
    private TimeoutList<PeriodicRetryItem<T>> m_timeoutList = new TimeoutList<PeriodicRetryItem<T>>();
    private double m_timeout = 10;

    /// <summary>
    /// A callback invoked when an item exceeds the retry count and is removed from the list.
    /// </summary>
    public RetryItemRemovedCallback<T> OnItemRemoved;

    /// <summary>
    /// A callback invoked when an item timeout has occurred but the retry count has not been exceeded.
    /// </summary>
    public TimeoutItemExpired<T> OnItemRetry;

    /// <summary>
    /// Gets or sets the item at the indicated index in the retry list
    /// </summary>
    /// <param name="i">The index of the item in the list.</param>
    /// <returns>The item at the index.</returns>
    public T this[int i]
    {
      get { return m_timeoutList[i].Item; }
      set { m_timeoutList[i].Item = value; }
    }

    /// <summary>
    /// Initializes a <see cref="PeriodicRetryList{T}"/> with the indicated timeout.
    /// </summary>
    /// <param name="timeout">The timeout for all items added to this list.</param>
    public PeriodicRetryList(double timeout)
    {
      m_timeoutList.OnItemExpired += OnItemExpired;
      m_timeout = timeout;
    }

    /// <summary>
    /// Adds an item to the list.
    /// </summary>
    /// <param name="item">The item to add.</param>
    /// <param name="retries">The max number of times the retry callback will be invoked.</param>
    public void Add(T item, int retries)
    {
      m_timeoutList.Add(new PeriodicRetryItem<T>(item, retries), m_timeout);
    }

    /// <summary>
    /// Removes the item from the list.
    /// </summary>
    /// <param name="item">The item to remove.</param>
    public void Remove(T item)
    {
      PeriodicRetryItem<T> rItem = m_timeoutList.Find(x => x.Item.Equals(item));
      if (rItem != null)
      {
        m_timeoutList.Remove(rItem);
      }
    }

    /// <summary>
    /// Removes the item at the specified index.
    /// </summary>
    /// <param name="index">The index of the item to remove.</param>
    public void RemoveAt(int index)
    {
      m_timeoutList.RemoveAt(index);
    }

    /// <summary>
    /// Searches for an item that matches the condition specified.
    /// </summary>
    /// <param name="match">The predicate that contains the matching conditions for the item.</param>
    /// <returns>The first item found that matches the conditions.</returns>
    public T Find(Predicate<T> match)
    {
      PeriodicRetryItem<T> rItem = m_timeoutList.Find(x => match.Invoke(x.Item));
      if (rItem != null)
      {
        return rItem.Item;
      }

      return default(T);
    }

    /// <summary>
    /// Updates the times and expires any items in the list who have exceeded that time.
    /// </summary>
    /// <param name="time">The object containing update timing information.</param>
    public void Update(UpdateTime time)
    {
      m_timeoutList.Update(time);
    }

    /// <summary>
    /// Callback that determines whether an item should retry or be removed and invokes the corresponding callback.
    /// </summary>
    /// <param name="retryItem">The item that expired.</param>
    private void OnItemExpired(PeriodicRetryItem<T> retryItem)
    {
      if (retryItem.Retries < retryItem.MaxRetries)
      {
        ++retryItem.Retries;
        m_timeoutList.Add(retryItem, m_timeout);
        OnItemRetry?.Invoke(retryItem.Item);
      }
      else
      {
        OnItemRemoved?.Invoke(retryItem.Item);
      }
    }
  }
}
