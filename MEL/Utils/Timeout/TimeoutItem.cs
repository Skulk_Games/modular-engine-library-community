﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skulk.MEL.Utils
{
  /// <summary>
  /// A class that holds a generic item for a specified timeout period.
  /// </summary>
  /// <typeparam name="T">The object to hold as the item.</typeparam>
  class TimeoutItem<T>
  {
    private double m_time;
    private double m_timeout;
    private T m_item;

    public double Time { get => m_time; set => m_time = value; }
    public double Timeout { get => m_timeout; set => m_timeout = value; }
    public T Item { get => m_item; set => m_item = value; }
    public bool Expired { get => m_time >= m_timeout; }

    public TimeoutItem(T item, double timeout)
    {
      m_item = item;
      m_timeout = timeout;
    }

    public void Update(double deltaTime)
    {
      m_time += deltaTime;
    }
  }
}
