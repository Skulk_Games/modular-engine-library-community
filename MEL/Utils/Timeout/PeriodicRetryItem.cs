﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skulk.MEL.Utils
{
  class PeriodicRetryItem<T>
  {
    private int m_retries;
    private int m_maxRetries;
    private T m_item;

    public int Retries { get => m_retries; set => m_retries = value; }
    public int MaxRetries { get => m_maxRetries; set => m_maxRetries = value; }
    public T Item { get => m_item; set => m_item = value; }

    public PeriodicRetryItem(T item, int retries)
    {
      m_item = item;
      m_maxRetries = retries;
    }
  }
}
