﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2020  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System.Collections.Generic;

namespace Skulk.MEL.Utils
{
  /// <summary>
  /// Holds a queue of items that expire individually in sequence after a specified time
  /// </summary>
  /// <typeparam name="T">The type of the item to hold in the queue</typeparam>
  /// <remarks>
  /// This is designed to work within the MEL framework with periodic update calls.
  /// This class is designed to be safe for sharing access across threads.
  /// </remarks>
  public class TimeoutQueue<T> : IRealtimeUpdate
  {
    private const double DEFAULT_TIMEOUT = 10;
    private Queue<TimeoutItem<T>> m_queue;

    /// <summary>
    /// An event callback invoked when an item expires and is removed from the queue
    /// </summary>
    public event TimeoutItemExpired<T> OnItemExpired;

    /// <summary>
    /// The number of items currently in the queue
    /// </summary>
    public int Count { get => GetCount(); }

    /// <summary>
    /// Initializes a new empty timeout queue with the default initial capacity
    /// </summary>
    public TimeoutQueue()
    {
      m_queue = new Queue<TimeoutItem<T>>();
    }

    /// <summary>
    /// Initializes a new timeout queue with the specified initial capacity
    /// </summary>
    /// <param name="capacity">The initial capacity of the queue</param>
    public TimeoutQueue(int capacity)
    {
      m_queue = new Queue<TimeoutItem<T>>(capacity);
    }

    /// <summary>
    /// Adds an item to the end of the timeout queue
    /// </summary>
    /// <param name="item">The item to add</param>
    /// <param name="timeout">The max time the item is allowed at the front of the queue before timing out</param>
    public void Enqueue(T item, double timeout = DEFAULT_TIMEOUT)
    {
      lock (m_queue)
      {
        m_queue.Enqueue(new TimeoutItem<T>(item, timeout));
      }
    }

    /// <summary>
    /// Removes and returns the object at the beginning of the queue.
    /// </summary>
    /// <returns>The object at the beginning of the queue or the default value for the type if no item exists</returns>
    public virtual T Dequeue()
    {
      lock (m_queue)
      {
        if (m_queue.Count > 0)
        {
          return m_queue.Dequeue().Item;
        }
      }

      return default(T);
    }

    /// <summary>
    /// Returns the object at the beginning of the queue without removing it
    /// </summary>
    /// <returns>The object at the beginning of the queue or the default value for the type if no item exists</returns>
    public virtual T Peek()
    {
      lock (m_queue)
      {
        if (m_queue.Count > 0)
        {
          return m_queue.Peek().Item;
        }
      }

      return default(T);
    }

    /// <summary>
    /// Resets the elapsed time of the item at the beginning of the queue
    /// </summary>
    public virtual void Reset()
    {
      lock (m_queue)
      {
        if (m_queue.Count > 0)
        {
          m_queue.Peek().Time = 0;
        }
      }
    }

    /// <summary>
    /// Updates the time of the item at the beginning of the queue and removes it if it has expired.
    /// This should be called periodically.
    /// </summary>
    /// <remarks>
    /// This does not apply time deltas across multiple items so a maximum of one item will be removed per call
    /// </remarks>
    /// <param name="time">The object containing update timing information</param>
    public virtual void Update(UpdateTime time)
    {
      TimeoutItem<T> expiredItem = null;
      lock (m_queue)
      {
        if (m_queue.Count > 0)
        {
          m_queue.Peek().Update(time.DeltaSeconds);
          if (m_queue.Peek().Expired)
          {
            expiredItem = m_queue.Dequeue();
          }
        }
      }

      if (expiredItem != null)
      {
        OnItemExpired?.Invoke(expiredItem.Item);
      }
    }

    /// <summary>
    /// Copies the queue elements to a new array
    /// </summary>
    /// <returns>The new array of copied queue elements</returns>
    public virtual T[] ToArray()
    {
      lock (m_queue)
      {
        T[] items = new T[m_queue.Count];
        TimeoutItem<T>[] timeoutItems = m_queue.ToArray();
        for (int i = 0; i < timeoutItems.Length; ++i)
        {
          items[i] = timeoutItems[i].Item;
        }

        return items;
      }
    }

    /// <summary>
    /// Gets the number of items in the queue
    /// </summary>
    /// <returns>The number of items in the queue</returns>
    public int GetCount()
    {
      lock (m_queue)
      {
        return m_queue.Count;
      }
    }
  }
}
