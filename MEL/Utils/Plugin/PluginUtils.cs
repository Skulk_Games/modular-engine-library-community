﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2020  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Skulk.MEL.Utils
{
  /// <summary>
  /// Class containing utility methods for dealing with runtime discoverable assemblies and modules (plugins)
  /// </summary>
  public static class PluginUtils
  {
    /// <summary>
    /// Attempts to load all the DLL assembly files in a specified directory
    /// </summary>
    /// <param name="directory">The directory to search for DLL assemblies to load</param>
    /// <returns>The list of loaded assemblies</returns>
    public static List<Assembly> LoadFromDirectory(string directory)
    {
      List<Assembly> assemblies = new List<Assembly>();

      if (Directory.Exists(directory))
      {
        string fullPath = Path.GetFullPath(directory);
        string[] files = Directory.GetFiles(directory);
        for (int i = 0; i < files.Length; ++i)
        {
          try
          {
            string file = Path.Combine(fullPath, files[i]);
            if (Path.GetExtension(file) == ".dll")
            {
              Assembly assembly = Assembly.LoadFrom(file);
              if (assembly != null)
              {
                assemblies.Add(assembly);
              }
            }
          }
          catch
          {
            // Failed to load a dll, skip...
          }
        }
      }

      return assemblies;
    }

    /// <summary>
    /// Finds all the module types within an assembly with the specified parent application type
    /// </summary>
    /// <typeparam name="T">The type of the module parent application to search for</typeparam>
    /// <param name="assembly">The assembly to search for modules</param>
    /// <returns>The list of modules compatible with the specified parent application class type</returns>
    public static List<Type> FindModules<T>(Assembly assembly) where T : class, IModularObject
    {
      return FindModules(typeof(GenericModule<T>), assembly);
    }

    /// <summary>
    /// Finds all the types within an assembly that implement the <see cref="IModule"/> interface.
    /// </summary>
    /// <param name="assembly">The assembly to search for modules</param>
    /// <returns>The list of modules found in the assembly</returns>
    public static List<Type> FindModules(Assembly assembly)
    {
      return FindModules(typeof(IModule), assembly);
    }

    /// <summary>
    /// Finds all the types within an assembly that are assignable to a specified type.
    /// </summary>
    /// <param name="assignType">The type to test if assembly types are assignable to</param>
    /// <param name="assembly">The assembly to search for compatible types</param>
    /// <returns>The list of compatible types found in the assembly</returns>
    public static List<Type> FindModules(Type assignType, Assembly assembly)
    {
      List<Type> modules = new List<Type>();

      Type[] types = assembly.GetExportedTypes();
      for (int i = 0; i < types.Length; ++i)
      {
        if (assignType.IsAssignableFrom(types[i]))
        {
          modules.Add(types[i]);
        }
      }

      return modules;
    }
  }
}
