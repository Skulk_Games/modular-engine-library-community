﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace Skulk.MEL.Utils
{
  /// <summary>
  /// A command line parser that parses arguments for properties and fields with the <see cref="OptionAttribute"/> tag
  /// </summary>
  public class CommandLineParser
  {
    /// <summary>
    /// The spacing string to prepend before descriptions of options
    /// </summary>
    protected const string DESCRIPTION_SPACING = "                    ";

    /// <summary>
    /// The help option for displaying help text
    /// </summary>
    [Option('h', "help", "Displays this help text.")]
    public bool Help { get; set;}

    /// <summary>
    /// The application name
    /// </summary>
    private string m_appName;
    
    /// <summary>
    /// Default constructor
    /// </summary>
    public CommandLineParser()
    {
      // Empty
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="CommandLineParser"/> with the specified application name
    /// </summary>
    /// <param name="name">The name of the application</param>
    public CommandLineParser(string name)
    {
      m_appName = name;
    }

    /// <summary>
    /// Parses the command line arguments and sets the defined option values.
    /// </summary>
    /// <param name="args">The command line arguments to parse.</param>
    /// <returns>True if execution of the program should continue, false if it should exit.</returns>
    public virtual bool Parse(string[] args)
    {
      // Parse tokens
      List<OptionToken> tokens = OptionToken.ParseTokens(args);

      bool result = ParseTokens(tokens, null);
      if (Help || !result)
      {
        PrintHelp();
      }

      return result && !Help;
    }

    /// <summary>
    /// Prints the help text for the command line arguments
    /// </summary>
    public virtual void PrintHelp()
    {
      Console.WriteLine(" {0} Help Text:", m_appName);
      PrintHelp(null);
    }

    /// <summary>
    /// Parses the specified list of command line tokens and sets the values on this parser.
    /// </summary>
    /// <param name="tokens">The list of tokens to parse</param>
    /// <param name="caller">The ancestor class type to use while parsing, if null the most derived class type of this instance is used</param>
    /// <returns>True if the tokens were parsed successfully. False if there was an error parsing the tokens</returns>
    protected bool ParseTokens(List<OptionToken> tokens, Type caller = null)
    {
      // Setup recursive call to this method in order to support derived classes
      Type self = caller ?? GetType();
      MethodInfo method = self.BaseType?.GetMethod("ParseTokens", BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[] { typeof(List<OptionToken>), typeof(Type) }, null);
      bool success = true;
      if (method != null)
      {
        success = (bool)method.Invoke(this, new object[] { tokens, self.BaseType });
      }

      // Gather fields with options
      FieldInfo[] fields = self.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
      List<FieldInfo> optionFields = new List<FieldInfo>();
      for (int i = 0; i < fields.Length; ++i)
      {
        OptionAttribute attrib = fields[i].GetCustomAttribute<OptionAttribute>();
        if (attrib != null)
        {
          optionFields.Add(fields[i]);
        }
      }

      // Gather properties with options
      PropertyInfo[] properties = self.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly | BindingFlags.NonPublic);
      List<PropertyInfo> optionProperties = new List<PropertyInfo>();
      for (int i = 0; i < properties.Length; ++i)
      {
        OptionAttribute attrib = properties[i].GetCustomAttribute<OptionAttribute>();
        if (attrib != null)
        {
          optionProperties.Add(properties[i]);
        }
      }

      List<MemberInfo> requiredSets = new List<MemberInfo>();

      foreach (OptionToken token in tokens)
      {
        try
        {
          bool match = false;
          // Search for matching field
          for (int i = 0; i < optionFields.Count; ++i)
          {
            OptionAttribute optionAttrib = optionFields[i].GetCustomAttribute<OptionAttribute>();
            if (token.LongOption && optionAttrib.LongOption == token.Key)
            {
              match = true;
            }
            else if (!token.LongOption && optionAttrib.ShortOption == token.Key[0])
            {
              match = true;
            }

            if (match)
            {
              if (optionFields[i].FieldType == typeof(bool))
              {
                // Check if value is set
                if (token.Values == null)
                {
                  optionFields[i].SetValue(this, true);
                }
                // Check if value can be parsed to true/false
                else if (token.Values.Length >= 1 && bool.TryParse(token.Values[0].ToLower(), out bool flag))
                {
                  optionFields[i].SetValue(this, flag);
                }
                else
                {
                  // Unable to parse values. We will assume the presence of the flag makes it true
                  optionFields[i].SetValue(this, true);
                }
              }
              else if (optionFields[i].FieldType.IsArray)
              {
                Type elementType = optionFields[i].FieldType.GetElementType();
                Array arr = (Array)Activator.CreateInstance(optionFields[i].FieldType, token.Values.Length);

                for (int valIdx = 0; valIdx < arr.Length; valIdx++)
                {
                  arr.SetValue(TypeDescriptor.GetConverter(elementType).ConvertFromInvariantString(token.Values[valIdx]), valIdx);
                }

                optionFields[i].SetValue(this, arr);
              }
              else if (optionFields[i].FieldType.IsGenericType && optionFields[i].FieldType.GetGenericTypeDefinition() == typeof(List<>))
              {
                Type elementType = optionFields[i].FieldType.GetGenericArguments()[0];
                System.Collections.IList list = Activator.CreateInstance(optionFields[i].FieldType) as System.Collections.IList;
                for (int listIdx = 0; listIdx < token.Values.Length; ++listIdx)
                {
                  list.Add(TypeDescriptor.GetConverter(elementType).ConvertFromInvariantString(token.Values[listIdx]));
                }

                optionFields[i].SetValue(this, list);
              }
              else
              {
                if (token.Values == null || token.Values.Length == 0)
                {
                  Console.WriteLine("Option: '{0}' is missing a value", token.Key);
                }
                else
                {
                  TypeConverter converter = TypeDescriptor.GetConverter(optionFields[i].FieldType);
                  optionFields[i].SetValue(this, converter.ConvertFromInvariantString(token.Values[0]));
                }
              }

              if (optionAttrib.Required)
              {
                requiredSets.Add(optionFields[i]);
              }

              // Break from field searching Jump to next token
              break;
            }
          }

          if (match)
          {
            continue;
          }

          // Search for matching property
          for (int i = 0; i < optionProperties.Count; ++i)
          {
            OptionAttribute optionAttrib = optionProperties[i].GetCustomAttribute<OptionAttribute>();
            if (token.LongOption && optionAttrib.LongOption == token.Key)
            {
              match = true;
            }
            else if (!token.LongOption && optionAttrib.ShortOption == token.Key[0])
            {
              match = true;
            }

            if (match)
            {
              if (optionProperties[i].PropertyType == typeof(bool))
              {
                // Check if value is set
                if (token.Values == null)
                {
                  optionProperties[i].SetValue(this, true);
                }
                // Check if value can be parsed to true/false
                else if (token.Values.Length >= 1 && bool.TryParse(token.Values[0].ToLower(), out bool flag))
                {
                  optionProperties[i].SetValue(this, flag);
                }
                else
                {
                  // Unable to parse values. We will assume the presence of the flag makes it true
                  optionProperties[i].SetValue(this, true);
                }
              }
              else if (optionProperties[i].PropertyType.IsArray)
              {
                Type elementType = optionProperties[i].PropertyType.GetElementType();
                Array arr = (Array)Activator.CreateInstance(optionProperties[i].PropertyType, token.Values.Length);

                for (int valIdx = 0; valIdx < arr.Length; valIdx++)
                {
                  arr.SetValue(TypeDescriptor.GetConverter(elementType).ConvertFromInvariantString(token.Values[valIdx]), valIdx);
                }

                optionProperties[i].SetValue(this, arr);
              }
              else if (optionProperties[i].PropertyType.IsGenericType && optionProperties[i].PropertyType.GetGenericTypeDefinition() == typeof(List<>))
              {
                Type elementType = optionProperties[i].PropertyType.GetGenericArguments()[0];
                System.Collections.IList list = Activator.CreateInstance(optionProperties[i].PropertyType) as System.Collections.IList;
                for (int listIdx = 0; listIdx < token.Values.Length; ++listIdx)
                {
                  list.Add(TypeDescriptor.GetConverter(elementType).ConvertFromInvariantString(token.Values[listIdx]));
                }

                optionProperties[i].SetValue(this, list);
              }
              else
              {
                if (token.Values == null || token.Values.Length == 0)
                {
                  Console.WriteLine("Option: '{0}' is missing a value", token.Key);
                }
                else
                {
                  TypeConverter converter = TypeDescriptor.GetConverter(optionProperties[i].PropertyType);
                  optionProperties[i].SetValue(this, converter.ConvertFromInvariantString(token.Values[0]));
                }
              }

              if (optionAttrib.Required)
              {
                requiredSets.Add(optionProperties[i]);
              }

              // Break from property searching Jump to next token
              break;
            }
          }
        }
        catch (Exception ex)
        {
          Console.WriteLine("Failed to set value for option: {0}", token.Key);
          Console.WriteLine(ex.Message);
          success = false;
        }
      }

      // Check that required options have been set
      for (int i = 0; i < optionFields.Count; ++i)
      {
        OptionAttribute attrib = optionFields[i].GetCustomAttribute<OptionAttribute>();
        if (attrib.Required && !requiredSets.Contains(optionFields[i]))
        {
          Console.WriteLine("Required option: '{0}' was not set", attrib.GetOptionFlags());
          success = false;
        }
      }
      for (int i = 0; i < optionProperties.Count; ++i)
      {
        OptionAttribute attrib = optionProperties[i].GetCustomAttribute<OptionAttribute>();
        if (attrib.Required && !requiredSets.Contains(optionProperties[i]))
        {
          Console.WriteLine("Required option: '{0}' was not set", attrib.GetOptionFlags());
          success = false;
        }
      }

      return success;
    }

    /// <summary>
    /// Prints the help text for the specified ancestor class type of this parser
    /// </summary>
    /// <param name="caller">The ancestor class type of this class to print help text for</param>
    protected void PrintHelp(Type caller = null)
    {
      // Setup recursive call to this method in order to support derived classes
      Type self = caller ?? GetType();
      MethodInfo method = self.BaseType?.GetMethod("PrintHelp", BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[] { typeof(Type) }, null);
      if (method != null)
      {
        method.Invoke(this, new object[] { self.BaseType });
      }

      // Check all fields for the ArgOptionAttribute
      FieldInfo[] fields = self.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
      foreach (FieldInfo field in fields)
      {
        object[] attribs = field.GetCustomAttributes(false);
        for (int i = 0; i < attribs.Length; i++)
        {
          OptionAttribute optionAttribute = attribs[i] as OptionAttribute;
          PrintOptionHelp(optionAttribute, field.GetValue(this));
        }
      }

      PropertyInfo[] properties = self.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly | BindingFlags.NonPublic);
      foreach (PropertyInfo property in properties)
      {
        object[] attribs = property.GetCustomAttributes(false);
        foreach (object attr in attribs)
        {
          OptionAttribute optionAttribute = attr as OptionAttribute;
          PrintOptionHelp(optionAttribute, property.GetValue(this, null));
        }
      }
    }

    /// <summary>
    /// Prints the help text for the specified option
    /// </summary>
    /// <param name="option">The option to print help text for</param>
    /// <param name="value">The value of the object this option describes</param>
    protected virtual void PrintOptionHelp(OptionAttribute option, object value)
    {
      if (option != null)
      {
        ConsoleColor color = Console.ForegroundColor;
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.WriteLine(option.GetOptionFlags());
        Console.Write(DESCRIPTION_SPACING);

        if (option.Required)
        {
          Console.ForegroundColor = ConsoleColor.Red;
          Console.Write("[Required] ");
        }

        Console.ForegroundColor = color;
        Console.WriteLine(option.Description);

        if (option.PrintValue)
        {
          Console.WriteLine("{0}Default value = {1}", DESCRIPTION_SPACING, value ?? "null");
        }
      }
    }
  }
}
