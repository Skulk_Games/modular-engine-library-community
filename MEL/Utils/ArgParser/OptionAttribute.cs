﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skulk.MEL.Utils
{
  /// <summary>
  /// Attribute that defines a command line argument option
  /// </summary>
  [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
  public class OptionAttribute : Attribute
  {
    char m_shortOption;
    string m_longOption;
    string m_description;
    bool m_required;
    bool m_printValue;

    /// <summary>
    /// The short option flag used for this option, note that 'h' is reserved
    /// </summary>
    public char ShortOption { get => m_shortOption; set => m_shortOption = value; }

    /// <summary>
    /// The long option flag used for this option, note that 'help' is reserved
    /// </summary>
    public string LongOption { get => m_longOption; set => m_longOption = value; }

    /// <summary>
    /// The description text to print in the help text for this option
    /// </summary>
    public string Description { get => m_description; set => m_description = value; }

    /// <summary>
    /// If true the parser will return false when this option is not present
    /// </summary>
    public bool Required { get => m_required; set => m_required = value; }

    /// <summary>
    /// If true the parser will print the current value of this option in the help text
    /// </summary>
    public bool PrintValue { get => m_printValue; set => m_printValue = value; }

    /// <summary>
    /// Constructs an option with a short and long option
    /// </summary>
    /// <param name="shortOption">The short option character</param>
    /// <param name="longOption">The long option string</param>
    /// <param name="description">The description of the option</param>
    /// <param name="required">If this option is required to be present</param>
    public OptionAttribute(char shortOption, string longOption, string description, bool required = false)
      : this(longOption, description, required)
    {
      m_shortOption = shortOption;
    }

    /// <summary>
    /// Constructs an option attribute without a short option
    /// </summary>
    /// <param name="longOption">The long option string</param>
    /// <param name="description">The description of the option</param>
    /// <param name="required">If this option is required to be present</param>
    public OptionAttribute(string longOption, string description, bool required = false)
    {
      m_longOption = longOption;
      m_description = description;
      m_required = required;
    }

    /// <summary>
    /// Gets the flag text for the option.
    /// </summary>
    /// <returns>A string showing the ways the flag is set.</returns>
    public string GetOptionFlags()
    {
      if (char.IsLetterOrDigit(m_shortOption) && m_longOption != null)
      {
        return "-" + m_shortOption + " | --" + m_longOption;
      }
      else if (char.IsLetterOrDigit(m_shortOption))
      {
        return "-" + m_shortOption;
      }
      else if (m_longOption != null)
      {
        return "--" + m_longOption;
      }

      return null;
    }
  }
}
