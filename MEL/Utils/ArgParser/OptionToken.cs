﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2021  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;

namespace Skulk.MEL.Utils
{
  /// <summary>
  /// Describes a parsed command line argument option
  /// </summary>
  public class OptionToken
  {
    private bool m_longOption;
    private string m_key;
    private string[] m_values;

    /// <summary>
    /// The option key identifying this token
    /// </summary>
    public string Key { get => m_key; set => m_key = value; }

    /// <summary>
    /// The values associated with this token's option key
    /// </summary>
    public string[] Values { get => m_values; set => m_values = value; }

    /// <summary>
    /// Indicates whether this token has a long option key
    /// </summary>
    public bool LongOption { get => m_longOption; set => m_longOption = value; }

    /// <summary>
    /// Parses command line arguments into a list of <see cref="OptionToken"/> instances
    /// </summary>
    /// <param name="args">The command line arguments to parse</param>
    /// <returns>The list of tokens parsed from the arguments</returns>
    public static List<OptionToken> ParseTokens(string[] args)
    {
      List<OptionToken> tokens = new List<OptionToken>();

      int offset = 0;
      while (offset < args.Length)
      {
        OptionToken token = new OptionToken();
        if (token.Parse(args, ref offset))
        {
          tokens.Add(token);
        }
        else
        {
          Console.WriteLine("Failed to parse option: {0}", args[offset]);
          ++offset;
        }
      }

      return tokens;
    }

    /// <summary>
    /// Parses the specified arguments from the indicated offset
    /// </summary>
    /// <param name="args">The command line arguments to parse</param>
    /// <param name="offset">The index of the first argument to use when parsing</param>
    /// <returns>True if an option was successfully parsed, otherwise false</returns>
    public bool Parse(string[] args, ref int offset)
    {
      // Clean the arg
      if (!string.IsNullOrEmpty(args[offset]))
      {
        string arg = args[offset].Trim();
        if (arg.Contains("="))
        {
          // Value contained within the same index
          string[] fields = arg.Split('=');
          if (fields.Length == 2)
          {
            if (ParseKey(fields[0]))
            {
              fields[1] = fields[1].Trim('"', '\'');
              m_values = fields[1].Split(new string[] { " ", "\t", "\n" }, StringSplitOptions.RemoveEmptyEntries);
              ++offset;
              return true;
            }
            else
            {
              // Invalid key format
              Console.WriteLine("Invalid format for option key: {0}", fields[0]);
            }
          }
          else
          {
            // Invalid = format
            Console.WriteLine("Invalid option format for argument: {0}", arg);
          }
        }
        else
        {
          // Handle value in next index
          if (ParseKey(arg))
          {
            if (!m_longOption && m_values != null)
            {
              // Value already set
              ++offset;
              return true;
            }

            // Parse values in following indices
            List<string> values = new List<string>();
            for (++offset; offset < args.Length; ++offset)
            {
              if (!IsOption(args[offset]))
              {
                values.Add(args[offset].Trim('"', '\''));
              }
              else
              {
                break;
              }
            }

            if (values.Count > 0)
            {
              m_values = values.ToArray();
            }

            return true;
          }
          else
          {
            // Invalid key format
            Console.WriteLine("Invalid format for option key: {0}", arg);
          }
        }
      }
      else
      {
        // Invalid arg (null or empty)
        Console.WriteLine("Invalid argument: '{0}'", args[offset] ?? "NULL");
      }

      return false;
    }

    /// <summary>
    /// Parses the text for an option key
    /// </summary>
    /// <param name="key">The option key text to parse</param>
    /// <returns>True if the key was parsed successfully, otherwise false</returns>
    private bool ParseKey(string key)
    {
      if (!string.IsNullOrEmpty(key) && key[0] == '-')
      {
        if (key.Length > 1 && key[1] == '-')
        {
          m_longOption = true;
          m_key = key.Substring(2);
        }
        else
        {
          m_longOption = false;
          m_key = key[1].ToString();
          if (key.Length > 2)
          {
            m_values = new string[] { key.Substring(2) };
          }
        }

        return true;
      }

      return false;
    }

    /// <summary>
    /// Determines if the specified text is an option key
    /// </summary>
    /// <param name="option">The text to test</param>
    /// <returns>True if the text is an option key, otherwise false</returns>
    private bool IsOption(string option)
    {
      return !string.IsNullOrEmpty(option) && option[0] == '-';
    }
  }
}
