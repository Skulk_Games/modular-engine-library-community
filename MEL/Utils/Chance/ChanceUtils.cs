﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Skulk.MEL.Utils
{
  /// <summary>
  /// Contains utility methods for determining chance events
  /// </summary>
  public static class ChanceUtils
  {
    private static object ms_randLock = new object();
    private static Random ms_rand = new Random();

    /// <summary>
    /// Reseeds the random instance used in <see cref="ChanceUtils"/> with a cryptographically strong random seed
    /// </summary>
    public static void Reseed()
    {
      lock (ms_randLock)
      {
        using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
        {
          byte[] raw = new byte[sizeof(Int32)];
          rng.GetBytes(raw);
          ms_rand = new Random(BitConverter.ToInt32(raw, 0));
        }
      }
    }

    /// <summary>
    /// Gets a random integer within the specified bounds
    /// </summary>
    /// <param name="min">The inclusive minimum value</param>
    /// <param name="max">The exclusive maximum value</param>
    /// <returns>A random integer within the bounds</returns>
    public static int RandInt(int min, int max)
    {
      lock (ms_randLock)
      {
        return ms_rand.Next(min, max);
      }
    }

    /// <summary>
    /// Gets a random double within the specified bounds
    /// </summary>
    /// <param name="min">The inclusive minimum value</param>
    /// <param name="max">The exclusive maximum value</param>
    /// <returns>A random double within the bounds</returns>
    public static double RandDouble(double min, double max)
    {
      double spread = max - min;
      return min + (RandDouble() * spread);
    }

    /// <summary>
    /// Gets a random double greater than or equal to 0.0 and less than 1.0
    /// </summary>
    /// <returns>A random double greater than or equal to 0.0 and less than 1.0</returns>
    public static double RandDouble()
    {
      lock (ms_randLock)
      {
        return ms_rand.NextDouble();
      }
    }

    /// <summary>
    /// Randomly picks a choice based on the array of chances specified.
    /// The array should sum to 1.0
    /// </summary>
    /// <param name="chances">The array of chance values that sums to 1.0</param>
    /// <returns>The index of the successful chance, or -1 if an error occurred</returns>
    public static int RandChoice(double[] chances)
    {
      if (chances == null)
      {
        return -1;
      }

      double roll = RandDouble();
      double sum = 0;
      for (int i = 0; i < chances.Length; i++)
      {
        if (roll < sum + chances[i])
        {
          return i;
        }

        sum += chances[i];
      }

      return chances.Length - 1;
    }

    /// <summary>
    /// Generates a random UInt64 value
    /// </summary>
    /// <param name="min">The minimum generated value (inclusive)</param>
    /// <param name="max">The maximum generated value (exclusive)</param>
    /// <returns>A random UInt64 within the specified values</returns>
    public static UInt64 RandUInt64(UInt64 min, UInt64 max)
    {
      lock (ms_randLock)
      {
        UInt64 range = max - min;
        byte[] bytes = new byte[8];
        ms_rand.NextBytes(bytes);
        return min + (BitConverter.ToUInt64(bytes, 0) % range);
      }
    }

    /// <summary>
    /// Fills an array of bytes with random values
    /// </summary>
    /// <param name="bytes">The array of bytes to fill</param>
    public static void RandBytes(byte[] bytes)
    {
      if (bytes == null)
      {
        return;
      }

      lock (ms_randLock)
      {
        ms_rand.NextBytes(bytes);
      }
    }
  }
}
