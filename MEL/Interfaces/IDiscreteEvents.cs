﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

namespace Skulk.MEL
{
  /// <summary>
  /// Interface describing discrete event start and end methods.
  /// </summary>
  public interface IDiscreteEvents
  {
    /// <summary>
    /// Method called for discrete events as they begin.
    /// Primarily useful for when a turn starts.
    /// </summary>
    /// <param name="eventId">The event that is starting.</param>
    void EventStart(int eventId);

    /// <summary>
    /// Method called for discrete events as they end.
    /// Primarily useful for when a turn is ending.
    /// </summary>
    /// <param name="eventId">The event that is ending.</param>
    void EventEnd(int eventId);
  }
}
