﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skulk.MEL
{
  /// <summary>
  /// An application module that uses the static logger to log messages.
  /// </summary>
  public class StaticLog : Module, ILogger
  {
    /// <summary>
    /// Gets or sets the file where log messages will be written
    /// </summary>
    public string LogFile { get => LogController.StaticLog.LogFile; set => LogController.StaticLog.LogFile = value; }

    /// <summary>
    /// Gets or sets the enabled log level bitmask
    /// </summary>
    public byte LogLevel { get => LogController.StaticLog.LogLevel; set => LogController.StaticLog.LogLevel = value; }

    /// <summary>
    /// Gets or sets whether console log messages are enabled
    /// </summary>
    public bool EnableConsole { get => LogController.StaticLog.EnableConsole; set => LogController.StaticLog.EnableConsole = value; }

    /// <inheritdoc/>
    public LoggerStopMode StopMode { get => LogController.StaticLog.StopMode; set => LogController.StaticLog.StopMode = value; }

    /// <inheritdoc/>
    public double StopTime { get => LogController.StaticLog.StopTime; set => LogController.StaticLog.StopTime = value; }

    /// <summary>
    /// Initializes a new instance of the <see cref="StaticLog"/> class and ensures the static logger is started.
    /// </summary>
    public StaticLog(ModularApplication parent)
      : base(parent)
    {
      LogController.StaticLog.Start(true);
    }

    /// <summary>
    /// Assigns this module to be the parent application's logger.
    /// </summary>
    public override void Refresh()
    {
      Parent.Log = this;
    }

    /// <summary>
    /// Shuts down the static logger.
    /// </summary>
    public override void ModuleShutdown()
    {
      LogController.StaticLog.Stop();
    }

    /// <summary>
    /// Sets the file where static log messages will be written
    /// </summary>
    /// <param name="logFile">The file where static log messages will be written.</param>
    public void SetLogFile(string logFile)
    {
      LogController.StaticLog.SetLogFile(logFile);
    }

    /// <summary>
    /// Sets the static log levels that are enabled.
    /// </summary>
    /// <param name="levelMask">The bitfield of enabled log levels.</param>
    public void SetLogLevels(byte levelMask)
    {
      LogController.StaticLog.SetLogLevels(levelMask);
    }

    /// <summary>
    /// Enables or disables outputting log messages to the system console.
    /// </summary>
    /// <param name="enable">True to enable system console log output, false to disable it.</param>
    public void EnableConsoleOutput(bool enable)
    {
      LogController.StaticLog.EnableConsoleOutput(enable);
    }

    /// <summary>
    /// Logs a message to the static log as an error.
    /// </summary>
    /// <param name="msgFormat">The message format.</param>
    /// <param name="args">The argument values to use in the message format.</param>
    public void LogError(string msgFormat, params object[] args)
    {
      LogController.StaticLog.LogError(msgFormat, args);
    }

    /// <summary>
    /// Logs a message to the static log as a warning.
    /// </summary>
    /// <param name="msgFormat">The message format.</param>
    /// <param name="args">The argument values to use in the message format.</param>
    public void LogWarning(string msgFormat, params object[] args)
    {
      LogController.StaticLog.LogWarning(msgFormat, args);
    }

    /// <summary>
    /// Logs an information message to the static log.
    /// </summary>
    /// <param name="msgFormat">The message format.</param>
    /// <param name="args">The argument values to use in the message format.</param>
    public void LogInfo(string msgFormat, params object[] args)
    {
      LogController.StaticLog.LogInfo(msgFormat, args);
    }

    /// <summary>
    /// Logs a debug message to the static log.
    /// </summary>
    /// <param name="msgFormat">The message format.</param>
    /// <param name="args">The argument values to use in the message format.</param>
    public void LogDebug(string msgFormat, params object[] args)
    {
      LogController.StaticLog.LogDebug(msgFormat, args);
    }

    /// <summary>
    /// Logs a trace message to the static log.
    /// </summary>
    /// <param name="msgFormat">The message format.</param>
    /// <param name="args">The argument values to use in the message format.</param>
    public void LogTrace(string msgFormat, params object[] args)
    {
      LogController.StaticLog.LogTrace(msgFormat, args);
    }
  }
}
