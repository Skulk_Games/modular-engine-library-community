﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skulk.MEL
{
  /// <summary>
  /// A module that implements a log for the parent application.
  /// </summary>
  public class AppLog : Module, ILogger
  {
    private Logger m_log = new Logger();

    /// <summary>
    /// Gets or sets the file where log messages will be written
    /// </summary>
    public string LogFile { get => m_log.LogFile; set => m_log.LogFile = value; }

    /// <summary>
    /// Gets or sets the enabled log level bitmask
    /// </summary>
    public byte LogLevel { get => m_log.LogLevel; set => m_log.LogLevel = value; }

    /// <summary>
    /// Gets or sets whether console log messages are enabled
    /// </summary>
    public bool EnableConsole { get => m_log.EnableConsole; set => m_log.EnableConsole = value; }

    /// <inheritdoc/>
    public LoggerStopMode StopMode { get => m_log.StopMode; set => m_log.StopMode = value; }

    /// <inheritdoc/>
    public double StopTime { get => m_log.StopTime; set => m_log.StopTime = value; }

    /// <summary>
    /// Initializes a new instance of the <see cref="AppLog"/> class and starts a logger thread.
    /// </summary>
    public AppLog(ModularApplication parent)
      : base(parent)
    {
      m_log.Start();
    }

    /// <summary>
    /// Destructor, ensures logger thread stops.
    /// </summary>
    ~AppLog()
    {
      m_log.Stop();
    }

    /// <summary>
    /// Initializes the module and sets the parent application's logger to this module.
    /// </summary>
    public override void ModuleInit()
    {
      Parent.Log = this;
    }

    /// <summary>
    /// Starts up the logger if it is not already running
    /// </summary>
    public override void ModuleStartup()
    {
      if (!m_log.Running)
      {
        m_log.Start();
      }
    }

    /// <summary>
    /// Shuts down the logger.
    /// </summary>
    public override void ModuleShutdown()
    {
      m_log.Stop();
    }

    /// <summary>
    /// Enables or disables writing log messages to the system console.
    /// </summary>
    /// <param name="enable">True to enable console logging, false to disable it.</param>
    public void EnableConsoleOutput(bool enable)
    {
      m_log.EnableConsoleOutput(enable);
    }

    /// <summary>
    /// Logs a debug message.
    /// </summary>
    /// <param name="msgFormat">The formatted message.</param>
    /// <param name="args">The argument values for the formatted message.</param>
    public void LogDebug(string msgFormat, params object[] args)
    {
      m_log.LogDebug(msgFormat, args);
    }

    /// <summary>
    /// Logs an error message
    /// </summary>
    /// <param name="msgFormat">The formatted message.</param>
    /// <param name="args">The argument values for the formatted message.</param>
    public void LogError(string msgFormat, params object[] args)
    {
      m_log.LogError(msgFormat, args);
    }

    /// <summary>
    /// Logs an info message.
    /// </summary>
    /// <param name="msgFormat">The formatted message.</param>
    /// <param name="args">The argument values for the formatted message.</param>
    public void LogInfo(string msgFormat, params object[] args)
    {
      m_log.LogInfo(msgFormat, args);
    }

    /// <summary>
    /// Logs a trace message
    /// </summary>
    /// <param name="msgFormat">The formatted message.</param>
    /// <param name="args">The argument values for the formatted message.</param>
    public void LogTrace(string msgFormat, params object[] args)
    {
      m_log.LogTrace(msgFormat, args);
    }

    /// <summary>
    /// Logs a warning message.
    /// </summary>
    /// <param name="msgFormat">The formatted message.</param>
    /// <param name="args">The argument values for the formatted message.</param>
    public void LogWarning(string msgFormat, params object[] args)
    {
      m_log.LogWarning(msgFormat, args);
    }

    /// <summary>
    /// Sets the file where messages will be logged.
    /// </summary>
    /// <param name="logFile">The file to write log messages to.</param>
    public void SetLogFile(string logFile)
    {
      m_log.SetLogFile(logFile);
    }

    /// <summary>
    /// Sets the enabled log levels
    /// </summary>
    /// <param name="logLevels">The bitmask to enable log levels.</param>
    public void SetLogLevels(byte logLevels)
    {
      m_log.SetLogLevels(logLevels);
    }
  }
}
