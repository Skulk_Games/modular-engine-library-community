﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2020  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

namespace Skulk.MEL
{
  /// <summary>
  /// Describes the generic interface used by modules within an <see cref="IModularObject"/>
  /// </summary>
  public interface IModule
  {
    /// <summary>
    /// The parent application holding this module.
    /// </summary>
    IModularObject Parent { get; set; }

    /// <summary>
    /// A method that is called every time the parent object adds a new module.
    /// </summary>
    void Refresh();

    /// <summary>
    /// A method that is called when this module is first added to the parent object.
    /// </summary>
    void ModuleInit();

    /// <summary>
    /// A method that is called when the parent application is starting up.
    /// </summary>
    void ModuleStartup();

    /// <summary>
    /// A method that is called when the parent object is shutting down.
    /// </summary>
    void ModuleShutdown();

    /// <summary>
    /// An event method that is called periodically.
    /// </summary>
    /// <param name="updateTime">An object containing information about the application timing.</param>
    void Update(UpdateTime updateTime);

    /// <summary>
    /// Method called for discrete events as they begin.
    /// Primarily useful for when a turn starts.
    /// </summary>
    /// <param name="eventId">The event that is starting.</param>
    void EventStart(int eventId);

    /// <summary>
    /// Method called for discrete events as they end.
    /// Primarily useful for when a turn is ending.
    /// </summary>
    /// <param name="eventId">The event that is ending.</param>
    void EventEnd(int eventId);
  }
}
