﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Skulk.MEL
{
  /// <summary>
  /// Class that manages a dictionary of modules and implements the <see cref="IModularObject"/> interface.
  /// </summary>
  public class ModuleManager : IModularObject
  {
    private ILogger m_logger = null;
    private Dictionary<Type, List<IModule>> m_modules = new Dictionary<Type, List<IModule>>();

    /// <summary>
    /// A list of all module instances on this module, used internally, todo: does this make sense to have?
    /// </summary>
    private List<IModule> m_moduleList = new List<IModule>();

    /// <summary>
    /// Event periodically invoked to provide realtime update processing.
    /// </summary>
    private UpdateCallback m_onUpdate;

    /// <summary>
    /// Event triggered when the <see cref="EventStart(int)"/> method is called on this application.
    /// </summary>
    private DiscreteEventCallback m_onEventStart;

    /// <summary>
    /// Event triggered when the <see cref="EventEnd(int)"/> method is called on this application.
    /// </summary>
    private DiscreteEventCallback m_onEventEnd;

    /// <summary>
    /// Gets or sets the logger module of this application, uses the static log if no log module is attached.
    /// </summary>
    public ILogger Log { get => m_logger; set => m_logger = value; }

    /// <summary>
    /// The dictionary of modules.
    /// </summary>
    public Dictionary<Type, List<IModule>> Modules { get => m_modules; set => m_modules = value; }

    /// <summary>
    /// Event invoked when a module is added.
    /// </summary>
    public event EventHandler<IModule> ModuleAdded;

    /// <summary>
    /// Event invoked when a module is removed.
    /// </summary>
    public event EventHandler<IModule> ModuleRemoved;

    /// <summary>
    /// Initializes this module manager with the specified log instance.
    /// </summary>
    /// <param name="log">The log instance this object should use.</param>
    public ModuleManager(ILogger log = null)
    {
      m_logger = log;
    }

    /// <summary>
    /// Executes <see cref="IModule.ModuleStartup"/> for each module.
    /// </summary>
    public virtual void StartUp()
    {
      lock (m_modules)
      {
        foreach (KeyValuePair<Type, List<IModule>> pair in m_modules)
        {
          foreach (IModule module in pair.Value)
          {
            module.ModuleStartup();
          }
        }
      }
    }

    /// <summary>
    /// Executes <see cref="IModule.ModuleShutdown"/> for each module.
    /// </summary>
    public virtual void ShutDown()
    {
      lock (m_modules)
      {
        foreach (KeyValuePair<Type, List<IModule>> pair in m_modules)
        {
          foreach (IModule module in pair.Value)
          {
            module.ModuleShutdown();
          }
        }
      }
    }

    /// <summary>
    /// Calls update on all modules.
    /// </summary>
    /// <param name="updateTime">The object containing information about the application time.</param>
    public virtual void Update(UpdateTime updateTime)
    {
      m_onUpdate?.Invoke(updateTime);
    }

    /// <summary>
    /// Adds a module to this application.
    /// </summary>
    /// <param name="module">The module to add to this application.</param>
    /// <returns>True if the module was added, false otherwise.</returns>
    public virtual bool AddModule(IModule module)
    {
      bool added = false;
      lock (m_modules)
      {
        if (!m_modules.ContainsKey(module.GetType()))
        {
          m_modules.Add(module.GetType(), new List<IModule>() { module });
          added = true;
        }
        // Still enforce unique instances
        else if (!m_modules[module.GetType()].Contains(module))
        {
          m_modules[module.GetType()].Add(module);
          added = true;
        }

        if (added)
        {
          module.Parent = this;
          m_onUpdate += module.Update;
          m_onEventStart += module.EventStart;
          m_onEventEnd += module.EventEnd;
        }
        else
        {
          return false;
        }
      }

      if (added)
      {
        Refresh();
        module.ModuleInit();
        try
        {
          ModuleAdded?.Invoke(this, module);
        }
        catch (Exception ex)
        {
          if (Log != null)
          {
            Log.LogError("Exception in module added event: '{0}'", ex.Message);
            Log.LogTrace("{0}", ex.StackTrace);
          }
        }
      }

      return added;
    }

    /// <summary>
    /// Removes all modules of the specified type from this application.
    /// </summary>
    /// <param name="moduleType">The type of the modules to remove</param>
    /// <returns>True if any modules of the specified type were removed, false otherwise</returns>
    public virtual bool RemoveModule(Type moduleType)
    {
      List<IModule> modules = null;
      lock (m_modules)
      {
        if (m_modules.ContainsKey(moduleType))
        {
          modules = m_modules[moduleType];
          foreach (IModule module in modules)
          {
            m_onEventStart -= module.EventStart;
            m_onEventEnd -= module.EventEnd;
            m_onUpdate -= module.Update;
          }

          m_modules.Remove(moduleType);
        }
      }

      if (modules != null)
      {
        Refresh();
        foreach (IModule module in modules)
        {
          module.ModuleShutdown();
          try
          {
            ModuleRemoved?.Invoke(this, module);
          }
          catch (Exception ex)
          {
            if (Log != null)
            {
              Log.LogError("Exception in ModuleRemoved event: '{0}'", ex.Message);
              Log.LogTrace("{0}", ex.StackTrace);
            }
          }
        }
        return true;
      }

      return false;
    }

    /// <summary>
    /// Removes all modules of the specified type from this application.
    /// </summary>
    /// <typeparam name="T">The type of the modules to remove</typeparam>
    /// <returns>True if any module of the specified type was removed, false otherwise</returns>
    public virtual bool RemoveModule<T>()
    {
      return RemoveModule(typeof(T));
    }

    /// <summary>
    /// Removes all modules of the specified type name from this application.
    /// </summary>
    /// <param name="moduleTypeName">The type name of the modules to remove</param>
    /// <returns>True if any module of the specified type was removed, false otherwise</returns>
    public virtual bool RemoveModule(string moduleTypeName)
    {
      Type moduleType = Type.GetType(moduleTypeName);
      if (moduleType != null)
      {
        return RemoveModule(moduleType);
      }

      return false;
    }

    /// <summary>
    /// Removes the specified module instance from this application
    /// </summary>
    /// <param name="module">The module instance to remove</param>
    /// <returns>True if the module was removed, false otherwise</returns>
    public virtual bool RemoveModule(IModule module)
    {
      bool removed = false;
      lock (m_modules)
      {
        if (m_modules.ContainsKey(module.GetType()))
        {
          List<IModule> list = m_modules[module.GetType()];
          if (list.Remove(module))
          {
            if (list.Count == 0)
            {
              m_modules.Remove(module.GetType());
            }

            m_onUpdate -= module.Update;
            m_onEventStart -= module.EventStart;
            m_onEventEnd -= module.EventEnd;
            removed = true;
          }
          else
          {
            Log?.LogError("Unable to remove module: {0} it doesn't match the instance of the module held by this application", module.GetType());
          }
        }
      }

      if (removed)
      {
        // Call shutdown on the removed module
        module.ModuleShutdown();
        Refresh();
        try
        {
          ModuleRemoved?.Invoke(this, module);
        }
        catch (Exception ex)
        {
          if (Log != null)
          {
            Log.LogError("Exception in ModuleRemoved event: '{0}'", ex.Message);
            Log.LogTrace("{0}", ex.StackTrace);
          }
        }
      }

      return removed;
    }

    /// <summary>
    /// Attempts to get the first module of the specified type on this application.
    /// </summary>
    /// <typeparam name="T">The type of the module to get.</typeparam>
    /// <returns>The first module of the specified type or null if none exists.</returns>
    public virtual T GetModule<T>() where T : IModule
    {
      IModule module = GetModule(typeof(T));
      if (module != null)
      {
        return (T)module;
      }

      return default(T);
    }

    /// <summary>
    /// Attempts to get the first module with the specified type on this application.
    /// </summary>
    /// <param name="moduleType">The type of the module to get</param>
    /// <returns>The first module of the specified type or null if none exists</returns>
    public virtual IModule GetModule(Type moduleType)
    {
      return GetModules(moduleType)?[0];
    }

    /// <summary>
    /// Attempts to get a module with the specified type name.
    /// </summary>
    /// <param name="moduleTypeName">The name of the module type to get</param>
    /// <returns>The module of the specified type or null if no matching module exists</returns>
    public virtual IModule GetModule(string moduleTypeName)
    {
      Type moduleType = Type.GetType(moduleTypeName);
      if (moduleType != null)
      {
        return GetModule(moduleType);
      }

      return null;
    }

    /// <summary>
    /// Attempts to get the list of modules with the specified type on this application.
    /// </summary>
    /// <param name="moduleType">The type of the modules to get</param>
    /// <returns>The list of modules of the specified type or null if none exist</returns>
    public virtual List<IModule> GetModules(Type moduleType)
    {
      lock (m_modules)
      {
        if (m_modules.TryGetValue(moduleType, out List<IModule> modules))
        {
          return modules;
        }
      }

      return null;
    }

    /// <summary>
    /// Method called when a discrete event starts.
    /// </summary>
    /// <param name="eventId">The event that is starting.</param>
    public virtual void EventStart(int eventId)
    {
      m_onEventStart?.Invoke(eventId);
      IEnumerable<IModule> modules = GetModules();
      foreach (IModule module in modules)
      {
        module.EventStart(eventId);
      }
    }

    /// <summary>
    /// Method called for when a discrete event is ending.
    /// </summary>
    /// <param name="eventId">The event that is ending.</param>
    public virtual void EventEnd(int eventId)
    {
      IEnumerable<IModule> modules = GetModules();
      foreach (IModule module in modules)
      {
        module.EventEnd(eventId);
      }
    }

    /// <summary>
    /// Calls the <see cref="IModule.Refresh"/> method on all modules held by this application.
    /// </summary>
    private void Refresh()
    {
      // Get a shallow copied list of modules so that we can't deadlock when calling refresh
      IEnumerable<IModule> modules = GetModules();

      // Refresh each module
      foreach (IModule module in modules)
      {
        module.Refresh();
      }
    }

    /// <summary>
    /// Gets a shallow copied list containing a reference to all the modules on this manager.
    /// </summary>
    /// <returns>The list of all modules held on this manager.</returns>
    public IEnumerable<IModule> GetModules()
    {
      List<IModule> modules = new List<IModule>();
      lock (m_modules)
      {
        foreach (List<IModule> list in m_modules.Values)
        {
          modules.AddRange(list);
        }
      }

      return modules;
    }
  }
}
