﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

namespace Skulk.MEL
{
  /// <summary>
  /// The base class implementing core functionality and methods for a module that is added to an object.
  /// </summary>
  public class Module : IModule
  {
    private IModularObject m_parent;

    /// <summary>
    /// Gets or sets the parent object that owns this module.
    /// </summary>
    public IModularObject Parent { get => m_parent; set => m_parent = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    /// <remarks>
    /// The <see cref="Parent"/> property should be set before starting.
    /// </remarks>
    public Module()
    {
      // Empty
    }

    /// <summary>
    /// Initializes a new instance of the module with the specified parent.
    /// </summary>
    /// <param name="parent">The parent object that holds this module.</param>
    public Module(IModularObject parent)
    {
      m_parent = parent;
    }

    /// <summary>
    /// A method that is called every time the parent object adds a new module.
    /// </summary>
    public virtual void Refresh()
    {
      // Empty
    }

    /// <summary>
    /// A method that is called when this module is first added to the parent object.
    /// </summary>
    public virtual void ModuleInit()
    {
      // Empty
    }

    /// <summary>
    /// A method that is called when the parent application is starting up.
    /// </summary>
    public virtual void ModuleStartup()
    {
      // Empty
    }

    /// <summary>
    /// A method that is called when the parent object is shutting down.
    /// </summary>
    public virtual void ModuleShutdown()
    {
      // Empty
    }

    /// <summary>
    /// An event method that is called periodically.
    /// </summary>
    /// <param name="updateTime">The object containing information about the application time.</param>
    public virtual void Update(UpdateTime updateTime)
    {
      // Empty
    }

    /// <summary>
    /// Method called for discrete events as they begin.
    /// Primarily useful for when a turn starts.
    /// </summary>
    /// <param name="turn">The event that is starting.</param>
    public virtual void EventStart(int turn)
    {
      // Empty
    }

    /// <summary>
    /// Method called for discrete events as they end.
    /// Primarily useful for when a turn is ending.
    /// </summary>
    /// <param name="turn">The event that is ending.</param>
    public virtual void EventEnd(int turn)
    {
      // Empty
    }
  }
}
