﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2020  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

namespace Skulk.MEL
{
  /// <summary>
  /// A generic module that can reference a specified parent application type.
  /// </summary>
  /// <typeparam name="T">The class type of the parent object.</typeparam>
  public class GenericModule<T> : Module where T : class, IModularObject
  {
    private T m_parent;

    /// <summary>
    /// Gets or sets the typed parent application holding this module.
    /// </summary>
    public T ParentApplication { get => m_parent; set { m_parent = value; Parent = value; } }

    /// <summary>
    /// Default constructor
    /// </summary>
    /// <remarks>
    /// The <see cref="Module.Parent"/> property should be set before starting.
    /// </remarks>
    public GenericModule()
    {
      // Empty
    }

    /// <summary>
    /// Initializes a new instance of the module with the specified parent.
    /// </summary>
    /// <param name="parent">The parent application that holds this module.</param>
    public GenericModule(T parent) : base(parent)
    {
      m_parent = parent;
    }
  }
}
