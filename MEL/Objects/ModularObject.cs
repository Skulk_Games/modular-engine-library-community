﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
//
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;

namespace Skulk.MEL.Objects
{
  /// <summary>
  /// A class that provides the core modular architecture for an object.
  /// </summary>
  public class ModularObject : IModularObject, IRealtimeUpdate
  {
    private ModuleManager m_moduleManager;

    /// <summary>
    /// Gets or sets the logger instance to use on this object.
    /// </summary>
    public ILogger Log { get => m_moduleManager?.Log; set => m_moduleManager.Log = value; }

    /// <summary>
    /// Initializes this modular object with the specified log instance.
    /// </summary>
    /// <param name="log">The log instance to use.</param>
    public ModularObject(ILogger log = null)
    {
      m_moduleManager = new ModuleManager(log);
    }

    /// <summary>
    /// Adds the specified module onto this object.
    /// </summary>
    /// <param name="module">The module to add.</param>
    /// <returns>True if the module was successfully added, false otherwise.</returns>
    public bool AddModule(IModule module)
    {
      return m_moduleManager.AddModule(module);
    }

    /// <summary>
    /// Adds the specified modules onto this object.
    /// </summary>
    /// <param name="modules">The modules to add.</param>
    /// <returns>True if all the modules were added, false if any module failed to be added.</returns>
    public bool AddModules(params IModule[] modules)
    {
      if (modules != null)
      {
        bool result = true;
        foreach(IModule module in modules)
        {
          result &= AddModule(module);
        }

        return result;
      }

      return false;
    }

    /// <summary>
    /// Attempts to remove the indicated module.
    /// </summary>
    /// <param name="module">A module of the type to remove from this object.</param>
    /// <returns>True if a module was removed, false otherwise.</returns>
    public bool RemoveModule(IModule module)
    {
      return m_moduleManager.RemoveModule(module);
    }

    /// <summary>
    /// Attempts to remove a module of the indicated type.
    /// </summary>
    /// <typeparam name="M">The type of module to remove.</typeparam>
    /// <returns>True if a module was removed, false otherwise.</returns>
    public bool RemoveModule<M>()
    {
      return m_moduleManager.RemoveModule<M>();
    }

    /// <summary>
    /// Removes a module of the specified type from this application.
    /// </summary>
    /// <param name="moduleType">The type of the module to remove</param>
    /// <returns>True if a module of the specified type was removed, false otherwise</returns>
    public bool RemoveModule(Type moduleType)
    {
      return m_moduleManager.RemoveModule(moduleType);
    }

    /// <summary>
    /// Removes a module of the specified type name from this application.
    /// </summary>
    /// <param name="moduleTypeName">The type name of the module to remove</param>
    /// <returns>True if a module of the specified type was removed, false otherwise</returns>
    public bool RemoveModule(string moduleTypeName)
    {
      return m_moduleManager.RemoveModule(moduleTypeName);
    }

    /// <summary>
    /// Gets the module of the specified type on this object.
    /// </summary>
    /// <typeparam name="M">The class type of the module to get.</typeparam>
    /// <returns>The module on this object or null if no such module exists.</returns>
    public virtual M GetModule<M>() where M : IModule
    {
      return m_moduleManager.GetModule<M>();
    }

    /// <summary>
    /// Attempts to get the module with the specified type on this application.
    /// </summary>
    /// <param name="moduleType">The type of the module to get</param>
    /// <returns>The module of the specified type or null if none exists</returns>
    public IModule GetModule(Type moduleType)
    {
      return m_moduleManager.GetModule(moduleType);
    }

    /// <summary>
    /// Attempts to get a module with the specified type name.
    /// </summary>
    /// <param name="moduleTypeName">The name of the module type to get</param>
    /// <returns>The module of the specified type or null if no matching module exists</returns>
    public IModule GetModule(string moduleTypeName)
    {
      return m_moduleManager.GetModule(moduleTypeName);
    }

    /// <summary>
    /// Performs initialization on this object
    /// </summary>
    public virtual void ObjectInit()
    {
      // Empty
    }

    /// <summary>
    /// Performs shutdown actions on all the modules on this object.
    /// </summary>
    public virtual void ObjectShutdown()
    {
      m_moduleManager.ShutDown();
    }

    /// <summary>
    /// The periodic update event method that calls the update methods for all modules on this object.
    /// </summary>
    /// <param name="updateTime">The object containing time information for the application.</param>
    public virtual void Update(UpdateTime updateTime)
    {
      m_moduleManager.Update(updateTime);
    }

    /// <summary>
    /// Method called for discrete events as they begin.
    /// Primarily useful for when a turn starts.
    /// </summary>
    /// <param name="eventId">The event that is starting.</param>
    public virtual void EventStart(int eventId)
    {
      m_moduleManager.EventStart(eventId);
    }

    /// <summary>
    /// Method called for discrete events as they end.
    /// Primarily useful for when a turn is ending.
    /// </summary>
    /// <param name="eventId">The event that is ending.</param>
    public virtual void EventEnd(int eventId)
    {
      m_moduleManager.EventEnd(eventId);
    }

    /// <summary>
    /// Gets an enumerable collection of all the modules held on this object.
    /// </summary>
    /// <returns>An enumerable collection of all the modules on this object.</returns>
    public virtual IEnumerable<IModule> GetModules()
    {
      return m_moduleManager.GetModules();
    }

    /// <inheritdoc/>
    public virtual List<IModule> GetModules(Type type)
    {
      return m_moduleManager.GetModules(type);
    }
  }
}
