﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

namespace Skulk.MEL.Objects
{
  /// <summary>
  /// A typical implementation of a modular object in an engine.
  /// </summary>
  /// <typeparam name="T">The class type to use for the module parent type.</typeparam>
  public class GenericEngineObject<T> : GenericModularObject<T> where T : GenericModularObject<T>
  {
    private string m_name = null;
    private int m_typeId = 0;
    private int m_objectId = 0;

    /// <summary>
    /// Initializes a new instance of the <see cref="EngineObject"/> class with default values.
    /// </summary>
    public GenericEngineObject()
    {
      // Empty
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="EngineObject"/> class with the specified values.
    /// </summary>
    /// <param name="name">The name of this engine object.</param>
    /// <param name="typeId">The type identifier for this object.</param>
    /// <param name="objectId">The identifier for this object instance.</param>
    public GenericEngineObject(string name, int typeId = 0, int objectId = 0)
    {
      Name = name;
      TypeId = typeId;
      ObjectId = objectId;
    }

    /// <summary>
    /// Gets or sets the name of this object.
    /// </summary>
    public string Name { get => m_name; set => m_name = value; }

    /// <summary>
    /// Gets or sets the type identifier value of this object.
    /// </summary>
    public int TypeId { get => m_typeId; set => m_typeId = value; }

    /// <summary>
    /// Gets or sets the instance object identifier value of this object.
    /// </summary>
    public int ObjectId { get => m_objectId; set => m_objectId = value; }
  }
}
