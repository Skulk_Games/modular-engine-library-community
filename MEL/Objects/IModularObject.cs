﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2020  Ryan Steven Jaynes
//
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;

namespace Skulk.MEL
{
  /// <summary>
  /// Interface for an object that holds modules
  /// </summary>
  public interface IModularObject : IDiscreteEvents
  {
    /// <summary>
    /// Gets or sets the logger of this application.
    /// </summary>
    ILogger Log { get; set; }

    /// <summary>
    /// Adds a module to this application.
    /// </summary>
    /// <param name="module">The module to add to this application.</param>
    /// <returns>True if the module was added, false otherwise.</returns>
    bool AddModule(IModule module);

    /// <summary>
    /// Removes a module of the specified type from this application.
    /// </summary>
    /// <param name="moduleType">The type of the module to remove</param>
    /// <returns>True if a module of the specified type was removed, false otherwise</returns>
    bool RemoveModule(Type moduleType);

    /// <summary>
    /// Removes a module of the specified type from this application.
    /// </summary>
    /// <typeparam name="T">The type of the module to remove</typeparam>
    /// <returns>True if a module of the specified type was removed, false otherwise</returns>
    bool RemoveModule<T>();

    /// <summary>
    /// Removes a module of the specified type name from this application.
    /// </summary>
    /// <param name="moduleTypeName">The type name of the module to remove</param>
    /// <returns>True if a module of the specified type was removed, false otherwise</returns>
    bool RemoveModule(string moduleTypeName);

    /// <summary>
    /// Removes the specified module instance from this application
    /// </summary>
    /// <param name="module">The module instance to remove</param>
    /// <returns>True if the module was removed, false otherwise</returns>
    bool RemoveModule(IModule module);

    /// <summary>
    /// Attempts to get the module of the specified type on this application.
    /// </summary>
    /// <typeparam name="T">The type of the module to get.</typeparam>
    /// <returns>The module of the specified type or null if none exists.</returns>
    T GetModule<T>() where T : IModule;

    /// <summary>
    /// Attempts to get the module with the specified type on this application.
    /// </summary>
    /// <param name="moduleType">The type of the module to get</param>
    /// <returns>The module of the specified type or null if none exists</returns>
    IModule GetModule(Type moduleType);

    /// <summary>
    /// Attempts to get a module with the specified type name.
    /// </summary>
    /// <param name="moduleTypeName">The name of the module type to get</param>
    /// <returns>The module of the specified type or null if no matching module exists</returns>
    IModule GetModule(string moduleTypeName);

    /// <summary>
    /// Gets an enumerable collection of all the modules on this object.
    /// </summary>
    /// <returns>The enumerable collection of modules on this object.</returns>
    IEnumerable<IModule> GetModules();

    /// <summary>
    /// Attempts to get the list of modules with the specified type on this application.
    /// </summary>
    /// <param name="moduleType">The type of the modules to get</param>
    /// <returns>The list of modules of the specified type or null if none exist</returns>
    List<IModule> GetModules(Type moduleType);
  }
}
