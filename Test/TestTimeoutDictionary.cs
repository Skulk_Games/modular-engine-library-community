﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2022  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using NUnit.Framework;
using Skulk.MEL;
using Skulk.MEL.Utils;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Skulk.Test
{
  /// <summary>
  /// Tests for the <see cref="TimeoutDictionary{TKey, TItem}"/> class.
  /// </summary>
  public class TestTimeoutDictionary
  {
    private ConcurrentQueue<string> m_expiredNames = new ConcurrentQueue<string>();
    private PeriodicUpdateGenerator m_generator = new PeriodicUpdateGenerator(30);

    /// <summary>
    /// Tear down after each test.
    /// </summary>
    [TearDown]
    public void TearDown()
    {
      m_generator.Stop();
      m_expiredNames.Clear();
    }

    /// <summary>
    /// Test the methods of a <see cref="TimeoutDictionary{TKey, TItem}"/> with a single item.
    /// </summary>
    [Test]
    public void SingleItem()
    {
      TimeoutDictionary<int, string> names = new TimeoutDictionary<int, string>();
      names.OnItemExpired += HandleItemExpired;
      m_generator.OnUpdate += names.Update;
      m_generator.Start(true);
      string name = "bob";

      // Test adding and expiring
      names.Add(0, name, 0.1);
      System.Threading.Thread.Sleep(50);
      Assert.AreEqual(0, m_expiredNames.Count);
      Assert.IsTrue(names.ContainsKey(0));
      System.Threading.Thread.Sleep(100);
      Assert.AreEqual(1, m_expiredNames.Count);
      Assert.IsFalse(names.ContainsKey(0));

      Assert.IsTrue(m_expiredNames.TryDequeue(out string expiredName));
      Assert.AreEqual(name, expiredName);

      // Test adding and removing
      names.Add(0, name, 0.1);
      System.Threading.Thread.Sleep(50);
      Assert.IsTrue(names.Remove(0));
      Assert.AreEqual(0, m_expiredNames.Count);
      Assert.AreEqual(0, names.Count);
      System.Threading.Thread.Sleep(100);
      Assert.AreEqual(0, m_expiredNames.Count);

      // Test TryAdd
      Assert.IsTrue(names.TryAdd(0, name, 0.1));
      Assert.IsFalse(names.TryAdd(0, name, 0.1));

      // Test Get
      Assert.AreEqual(name, names.Get(0));
      Assert.AreEqual(name, names[0]);

      // Test TryGetValue
      Assert.IsTrue(names.TryGetValue(0, out string item));
      Assert.AreEqual(name, item);

      // Test Set
      string name2 = "bobby";
      names.Set(0, name2);
      Assert.AreEqual(name2, names.Get(0));
      names[0] = name;
      Assert.AreEqual(name, names[0]);

      // Test Reset
      for (int i = 0; i < 10; ++i)
      {
        names.Reset(0);
        System.Threading.Thread.Sleep(50);
      }

      Assert.IsTrue(names.ContainsKey(0));
      Assert.AreEqual(1, names.Count);
      Assert.AreEqual(name, names[0]);

      int[] keys = names.Keys;
      Assert.IsNotNull(keys);
      Assert.AreEqual(1, keys.Length);
      Assert.AreEqual(0, keys[0]);
      string[] items = names.Values;
      Assert.IsNotNull(items);
      Assert.AreEqual(1, items.Length);
      Assert.AreEqual(name, items[0]);

      KeyValuePair<int, string>[] pairs = names.ToArray();
      Assert.IsNotNull(pairs);
      Assert.AreEqual(1, pairs.Length);
      Assert.AreEqual(0, pairs[0].Key);
      Assert.AreEqual(name, pairs[0].Value);

      // Let item expire
      System.Threading.Thread.Sleep(70);
      Assert.AreEqual(0, names.Count);

      // Get missing item
      Assert.IsNull(names.Get(0));
      Assert.IsNull(names[0]);

      keys = names.Keys;
      Assert.IsNotNull(keys);
      Assert.AreEqual(0, keys.Length);
      items = names.Values;
      Assert.IsNotNull(items);
      Assert.AreEqual(0, items.Length);

      pairs = names.ToArray();
      Assert.IsNotNull(pairs);
      Assert.AreEqual(0, pairs.Length);
    }

    /// <summary>
    /// Handle the event invoked when items expire.
    /// </summary>
    /// <param name="item">The item value that expired.</param>
    private void HandleItemExpired(string item)
    {
      m_expiredNames.Enqueue(item);
    }
  }
}
