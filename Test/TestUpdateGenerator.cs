﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2020  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using NUnit.Framework;
using Skulk.MEL;
using System;
using System.Collections.Concurrent;
using System.Threading;

namespace Skulk.Test
{
  public class TestUpdateGenerator
  {
    private ConcurrentQueue<UpdateTime> m_updateQueue = new ConcurrentQueue<UpdateTime>();
    private double m_delaySecs = 0.1;

    public enum GeneratorType
    {
      DateTime,
      Periodic
    }

    [SetUp]
    public void TestSetUp()
    {
      m_updateQueue.Clear();
    }

    /// <summary>
    /// Tests the accuracy of different IUpdateGenerator types. The parameters used in each test case are indicative
    /// of the strengths of each type of generator.
    /// </summary>
    /// <param name="generatorType">The type of generator to use in the test</param>
    /// <param name="maxUpdate">The max frequency to run the generator at</param>
    /// <param name="timingAccuracy">The timing accuracy to enforce on the deltaTime used by the generator</param>
    /// <param name="countAccuracy">The count accuracy to enforce on the number of times an update is generated</param>
    [TestCase(GeneratorType.DateTime, 10, 0.95, 0.89)]
    [TestCase(GeneratorType.DateTime, 60, 0.95, 0.95)]
    [TestCase(GeneratorType.DateTime, 100, 0.95, 0.95)]
    [TestCase(GeneratorType.DateTime, 200, 0.95, 0.95)]
    [TestCase(GeneratorType.DateTime, 500, 0.95, 0.95)]
    [TestCase(GeneratorType.Periodic, 10, 0.95, 0.89)]
    [TestCase(GeneratorType.Periodic, 60, 0.95, 0.95)]
    [TestCase(GeneratorType.Periodic, 100, 0.98, 0.98)]
    [TestCase(GeneratorType.Periodic, 200, 0.98, 0.98)]
    [TestCase(GeneratorType.Periodic, 500, 0.98, 0.98)]
    public void TestGenerator(GeneratorType generatorType, double maxUpdate, double timingAccuracy, double countAccuracy)
    {
      double testDuration = 1;
      int testDurationMs = (int)(testDuration * 1000);

      IUpdateGenerator updateGenerator = null;
      switch(generatorType)
      {
        case GeneratorType.DateTime:
        {
          updateGenerator = new DateTimeUpdateGenerator();
        }
        break;

        case GeneratorType.Periodic:
        {
          updateGenerator = new PeriodicUpdateGenerator();
        }
        break;
      }

      Assert.IsNotNull(updateGenerator);
      updateGenerator.MaxUpdateHz = maxUpdate;
      updateGenerator.OnUpdate += OnUpdate;

      System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();

      updateGenerator.Start();
      Assert.IsTrue(updateGenerator.Running);
      watch.Start();
      Thread.Sleep(testDurationMs);
      updateGenerator.Stop();
      Assert.IsFalse(updateGenerator.Running);
      watch.Stop();
      Thread.Sleep((int)(1 / maxUpdate * 1000));

      // Calculate the expected count from the actual run time
      double elapsedSeconds = watch.Elapsed.TotalSeconds;
      double expectedCount = maxUpdate * watch.Elapsed.TotalSeconds;
      Console.WriteLine("Test ran {0}s", elapsedSeconds);

      // Pass 90% accuracy in timing
      double minCount = (expectedCount * countAccuracy);
      double maxCount = (expectedCount + (expectedCount * (1 - countAccuracy)));
      int updateCount = m_updateQueue.Count;
      Assert.IsTrue(updateCount >= minCount, "count: {0} is less than minimum: {1}", updateCount, minCount);
      Assert.IsTrue(updateCount <= maxCount, "count: {0} is greater than max: {1}", updateCount, maxCount);

      double deltaSum = 0;
      while (m_updateQueue.Count > 0)
      {
        Assert.IsTrue(m_updateQueue.TryDequeue(out UpdateTime time));
        deltaSum += time.DeltaSeconds;
      }

      double sumDeltaPercent = (deltaSum - elapsedSeconds) / elapsedSeconds;
      Assert.IsTrue(Math.Abs(sumDeltaPercent) < (1 - timingAccuracy), "Total delta sum time is: {0}, {1}% off from expected: {2}", deltaSum, sumDeltaPercent * 100, elapsedSeconds);
      Console.WriteLine("Duration is {0}% off", sumDeltaPercent * 100);

      double deltaPercent = (updateCount - expectedCount) / expectedCount;
      Console.WriteLine("Update count is {0}% off", deltaPercent * 100);
    }

    /// <summary>
    /// Test the behavior of update generators when the update call is slower than the max rate
    /// </summary>
    /// <param name="generatorType"></param>
    [TestCase(GeneratorType.Periodic, 0.89, 0.89)]
    [TestCase(GeneratorType.DateTime, 0.89, 0.89)]
    public void TestSlowUpdate(GeneratorType generatorType, double timingAccuracy, double countAccuracy)
    {
      double maxUpdate = 60;

      IUpdateGenerator updateGenerator = null;
      switch (generatorType)
      {
        case GeneratorType.DateTime:
        {
          updateGenerator = new DateTimeUpdateGenerator();
        }
        break;

        case GeneratorType.Periodic:
        {
          updateGenerator = new PeriodicUpdateGenerator();
        }
        break;
      }

      Assert.IsNotNull(updateGenerator);
      updateGenerator.MaxUpdateHz = maxUpdate;
      updateGenerator.OnUpdate += OnSlowUpdate;

      System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();

      updateGenerator.Start();
      Assert.IsTrue(updateGenerator.Running);
      watch.Start();
      Thread.Sleep(1000);
      updateGenerator.Stop();
      Assert.IsFalse(updateGenerator.Running);
      watch.Stop();
      Thread.Sleep(100);

      // Calculate the expected count from the actual run time
      double elapsedSeconds = watch.Elapsed.TotalSeconds;
      double expectedCount = watch.Elapsed.TotalSeconds / m_delaySecs;

      // Pass accuracy in count
      double minCount = (expectedCount * countAccuracy);
      double maxCount = (expectedCount + (expectedCount * (1 - countAccuracy)));
      int updateCount = m_updateQueue.Count;
      Assert.IsTrue(updateCount >= minCount, "count: {0} is less than minimum: {1}", updateCount, minCount);
      Assert.IsTrue(updateCount <= maxCount, "count: {0} is greater than max: {1}", updateCount, maxCount);

      Console.WriteLine("Test ran {0}s", elapsedSeconds);

      double deltaSum = 0;
      while (m_updateQueue.Count > 0)
      {
        Assert.IsTrue(m_updateQueue.TryDequeue(out UpdateTime time));
        deltaSum += time.DeltaSeconds;
      }

      double sumDeltaPercent = (deltaSum - elapsedSeconds) / elapsedSeconds;
      Assert.IsTrue(Math.Abs(sumDeltaPercent) < (1 - timingAccuracy), "Total delta sum time is: {0}, {1}% off from expected: {2}", deltaSum, sumDeltaPercent * 100, elapsedSeconds);
      Console.WriteLine("Duration is {0}% off", sumDeltaPercent * 100);

      double deltaPercent = (updateCount - expectedCount) / expectedCount;
      Console.WriteLine("Update count is {0}% off", deltaPercent * 100);
    }

    private void OnUpdate(UpdateTime updateTime)
    {
      m_updateQueue.Enqueue(updateTime);
    }

    private void OnSlowUpdate(UpdateTime updateTime)
    {
      // Slow update
      System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
      watch.Start();
      while (watch.Elapsed.TotalSeconds < m_delaySecs)
      {
        // Busy wait loop
      }

      watch.Stop();

      m_updateQueue.Enqueue(updateTime);
    }
  }
}
