﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using NUnit.Framework;
using Skulk.MEL;
using System.IO;
using System.Threading;

namespace Skulk.Test
{
  public class TestLogModules
  {
    private static ModularApplication ms_app;
    private const string STATIC_LOG = "testStaticLog.log";

    [SetUp]
    public void TestSetup()
    {
      ms_app = new ModularApplication();
      ms_app.Start();
    }

    [TearDown]
    public void TestTearDown()
    {
      ms_app.Stop();
      ms_app.Join();
    }

    [TestCase(LogLevels.ERROR, STATIC_LOG)]
    [TestCase(LogLevels.WARNING, STATIC_LOG)]
    [TestCase(LogLevels.INFO, STATIC_LOG)]
    [TestCase(LogLevels.DEBUG, STATIC_LOG)]
    [TestCase(LogLevels.TRACE, STATIC_LOG)]
    public void TestStaticLog(LogLevels logLevel, string logFile)
    {
      if (File.Exists(STATIC_LOG))
      {
        File.Delete(STATIC_LOG);
      }

      ms_app.AddModule(new StaticLog(ms_app));
      ms_app.GetModule<StaticLog>().SetLogFile(STATIC_LOG);
      ms_app.Log.SetLogLevels((byte)LogLevels.ERROR);

      string contents = "valid contents, 1, 2, 3, ...";
      ms_app.Log.SetLogFile(logFile);
      ms_app.Log.SetLogLevels((byte)logLevel);
      ms_app.Log.LogError(contents);
      ms_app.Log.LogWarning(contents);
      ms_app.Log.LogInfo(contents);
      ms_app.Log.LogDebug(contents);
      ms_app.Log.LogTrace(contents);

      Thread.Sleep(100);
      StreamReader fin = new StreamReader(File.Open(logFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
      string line = fin.ReadLine();
      Assert.IsTrue(line.Contains(contents));
      Assert.IsTrue(line.Contains(logLevel.ToString()));
      Assert.IsTrue(fin.EndOfStream);

      // Cleanup
      fin.Close();

      ms_app.Stop();
      ms_app.Join();
      File.Delete(logFile);
    }
  }
}
