﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2021  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using NUnit.Framework;
using Skulk.MEL.Utils;
using System.Collections.Generic;
using System.IO;

namespace Skulk.Test
{
  /// <summary>
  /// Simple CommandLineParser implementation for tests
  /// </summary>
  class Parser : CommandLineParser
  {
    [Option('p', "path", "Path to something")]
    public string Path { get; set; } = "path";

    [Option('e', "enabled", "Enables something")]
    public bool Enabled { get; set; } = false;

    [Option('r', "ports", "An array of port numbers")]
    public int[] Ports { get; set; }

    [Option('n', "numbers", "Numbers")]
    public List<int> Numbers { get; set; }

    [Option("name", "The name of something")]
    public string Name { get; set; }
  }

  class RequiredParser : CommandLineParser
  {
    [Option('n', "name", "Name", true)]
    public string Name { get; set; }

    [Option('i', "id", "Identifier", true)]
    public int Id { get; set; }

    [Option('o', "option", "Optional value")]
    public int Option { get; set; }
  }

  class FieldParser : CommandLineParser
  {
    [Option('n', "name", "Name", false)]
    public string Name;

    [Option('d', "do-stuff", "Does stuff", PrintValue = true)]
    public bool DoStuff;

    public FieldParser(string appName) : base(appName)
    {
      // Empty
    }
  }

  /// <summary>
  /// Tests for <see cref="CommandLineParser"/> functionality
  /// </summary>
  public class TestCommandLineParser
  {
    [Test]
    public void TestQuotedArgs()
    {
      Parser parser = new Parser();
      string[] args = new string[]
      {
        "-p",
        "some/path/to/a/file.txt"
      };
      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual(args[1], parser.Path);

      string[] quotedArgs = new string[]
      {
        "-p",
        "\"some/path/to/a/file.txt\""
      };
      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual(args[1], parser.Path);
    }

    [Test]
    public void TestArrayValues()
    {
      Parser parser = new Parser();
      string[] args = new string[]
      {
        "-r",
        "1111",
        "2222",
        "3333"
      };

      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual(3, parser.Ports.Length);
      Assert.AreEqual(1111, parser.Ports[0]);
      Assert.AreEqual(2222, parser.Ports[1]);
      Assert.AreEqual(3333, parser.Ports[2]);

      args = new string[]
      {
        "-r=\"1111 2222 3333\""
      };

      parser = new Parser();
      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual(3, parser.Ports.Length);
      Assert.AreEqual(1111, parser.Ports[0]);
      Assert.AreEqual(2222, parser.Ports[1]);
      Assert.AreEqual(3333, parser.Ports[2]);

      args = new string[]
      {
        "--ports=\"1111 2222 3333\""
      };

      parser = new Parser();
      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual(3, parser.Ports.Length);
      Assert.AreEqual(1111, parser.Ports[0]);
      Assert.AreEqual(2222, parser.Ports[1]);
      Assert.AreEqual(3333, parser.Ports[2]);

      args = new string[]
      {
        "--ports",
        "1111",
        "2222",
        "3333"
      };

      parser = new Parser();
      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual(3, parser.Ports.Length);
      Assert.AreEqual(1111, parser.Ports[0]);
      Assert.AreEqual(2222, parser.Ports[1]);
      Assert.AreEqual(3333, parser.Ports[2]);
    }

    [Test]
    public void TestListValues()
    {
      string[] args = new string[]
      {
        "-n",
        "1",
        "2",
        "3"
      };

      Parser parser = new Parser();
      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual(3, parser.Numbers.Count);
      Assert.AreEqual(1, parser.Numbers[0]);
      Assert.AreEqual(2, parser.Numbers[1]);
      Assert.AreEqual(3, parser.Numbers[2]);

      args = new string[]
      {
        "--numbers",
        "1",
        "2",
        "3"
      };

      parser = new Parser();
      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual(3, parser.Numbers.Count);
      Assert.AreEqual(1, parser.Numbers[0]);
      Assert.AreEqual(2, parser.Numbers[1]);
      Assert.AreEqual(3, parser.Numbers[2]);

      args = new string[]
      {
        "--numbers=\"1 2 3\""
      };

      parser = new Parser();
      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual(3, parser.Numbers.Count);
      Assert.AreEqual(1, parser.Numbers[0]);
      Assert.AreEqual(2, parser.Numbers[1]);
      Assert.AreEqual(3, parser.Numbers[2]);

      args = new string[]
      {
        "--numbers",
        "0x1",
        "0x2",
        "0x3245"
      };

      parser = new Parser();
      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual(3, parser.Numbers.Count);
      Assert.AreEqual(1, parser.Numbers[0]);
      Assert.AreEqual(2, parser.Numbers[1]);
      Assert.AreEqual(0x3245, parser.Numbers[2]);
    }

    /// <summary>
    /// Tests the behavior of required options
    /// </summary>
    [Test]
    public void RequiredOptions()
    {
      string[] args = new string[0];
      RequiredParser parser = new RequiredParser();

      // Empty args should fail since required items are missing
      Assert.IsFalse(parser.Parse(args));

      args = new string[]
      {
        "-n",
        "bob"
      };

      // Still fail since only one set
      Assert.IsFalse(parser.Parse(args));

      args = new string[]
      {
        "-n",
        "bob",
        "-o",
        "1"
      };

      // Still fail since one still missing
      Assert.IsFalse(parser.Parse(args));

      args = new string[]
      {
        "-n",
        "bob",
        "-i",
        "0",
        "-o",
        "1"
      };

      // Should succeed
      Assert.IsTrue(parser.Parse(args));

      args = new string[]
      {
        "-n",
        "bob",
        "-i",
        "0"
      };

      // Should succeed
      Assert.IsTrue(parser.Parse(args));

      args = new string[]
      {
        "-n",
        "bob"
      };

      // Should fail
      Assert.IsFalse(parser.Parse(args));
    }

    /// <summary>
    /// Tests basic parsing of argument tokens
    /// </summary>
    [Test]
    public void TestTokenParsing()
    {
      string[] tokenString = new string[] 
      {
        "-oFile.txt",
        "-iInput.txt",
        "--version=3.1.0",
        "-c",
        "-n",
        "bob",
        "--name",
        "bob",
        "-f",
        "\"file-1\"",
        "file-2",
        "file-3",
        "--files=\"file-3 file-2 file-1\""
      };

      List<OptionToken> tokens = OptionToken.ParseTokens(tokenString);

      Assert.AreEqual(8, tokens.Count);

      // Test long option flag
      Assert.IsFalse(tokens[0].LongOption);
      Assert.IsFalse(tokens[1].LongOption);
      Assert.IsTrue(tokens[2].LongOption);
      Assert.IsFalse(tokens[3].LongOption);
      Assert.IsFalse(tokens[4].LongOption);
      Assert.IsTrue(tokens[5].LongOption);
      Assert.IsFalse(tokens[6].LongOption);
      Assert.IsTrue(tokens[7].LongOption);

      // Test key string
      Assert.AreEqual("o", tokens[0].Key);
      Assert.AreEqual("i", tokens[1].Key);
      Assert.AreEqual("version", tokens[2].Key);
      Assert.AreEqual("c", tokens[3].Key);
      Assert.AreEqual("n", tokens[4].Key);
      Assert.AreEqual("name", tokens[5].Key);
      Assert.AreEqual("f", tokens[6].Key);
      Assert.AreEqual("files", tokens[7].Key);

      // Test value string
      Assert.AreEqual(1, tokens[0].Values.Length);
      Assert.AreEqual("File.txt", tokens[0].Values[0]);
      Assert.AreEqual(1, tokens[1].Values.Length);
      Assert.AreEqual("Input.txt", tokens[1].Values[0]);
      Assert.AreEqual(1, tokens[2].Values.Length);
      Assert.AreEqual("3.1.0", tokens[2].Values[0]);
      Assert.AreEqual(null, tokens[3].Values);
      Assert.AreEqual(1, tokens[4].Values.Length);
      Assert.AreEqual("bob", tokens[4].Values[0]);
      Assert.AreEqual(1, tokens[5].Values.Length);
      Assert.AreEqual("bob", tokens[5].Values[0]);
      Assert.AreEqual(3, tokens[6].Values.Length);
      for (int i = 1; i < 4; ++i)
      {
        Assert.AreEqual("file-" + i.ToString(), tokens[6].Values[i - 1]);
      }
      Assert.AreEqual(3, tokens[7].Values.Length);
      for (int i = 1; i < 4; ++i)
      {
        Assert.AreEqual("file-" + (4 - i).ToString(), tokens[7].Values[i - 1]);
      }
    }

    /// <summary>
    /// Test that options can be specified without short options
    /// </summary>
    [Test]
    public void NoShortOption()
    {
      Parser parser = new Parser();
      string[] args = new string[]
      {
        "--name",
        "Bob"
      };
      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual(args[1], parser.Name);

      // Print help to see that it displays the help without a short option
      parser.PrintHelp();
    }

    /// <summary>
    /// Test parsing boolean values
    /// </summary>
    [Test]
    public void BoolParsing()
    {
      // Parser using properties
      Parser parser = new Parser();
      string[] args = new string[]
      {
        "-e=true"
      };

      // Set flag explicitly to true
      Assert.IsTrue(parser.Parse(args));
      Assert.IsTrue(parser.Enabled);

      // Set flag explicitly to false
      args[0] = "-e=false";
      Assert.IsTrue(parser.Parse(args));
      Assert.IsFalse(parser.Enabled);

      // Invalid flag value defaults to true
      args[0] = "-e=potatoes";
      Assert.IsTrue(parser.Parse(args));
      Assert.IsTrue(parser.Enabled);

      args[0] = "--enabled=false";
      Assert.IsTrue(parser.Parse(args));
      Assert.IsFalse(parser.Enabled);

      args[0] = "--enabled";
      Assert.IsTrue(parser.Parse(args));
      Assert.IsTrue(parser.Enabled);

      // Parser with fields
      FieldParser fieldParser = new FieldParser("Fields");
      args[0] = "-d=true";
      Assert.IsTrue(fieldParser.Parse(args));
      Assert.IsTrue(fieldParser.DoStuff);

      args[0] = "-d=false";
      Assert.IsTrue(fieldParser.Parse(args));
      Assert.IsFalse(fieldParser.DoStuff);

      args[0] = "-d=chickens";
      Assert.IsTrue(fieldParser.Parse(args));
      Assert.IsTrue(fieldParser.DoStuff);

      args[0] = "--do-stuff=false";
      Assert.IsTrue(fieldParser.Parse(args));
      Assert.IsFalse(fieldParser.DoStuff);

      args[0] = "--do-stuff";
      Assert.IsTrue(fieldParser.Parse(args));
      Assert.IsTrue(fieldParser.DoStuff);
    }

    [Test]
    public void FieldOptionsAndAppName()
    {
      string appName = "Field Parser";
      FieldParser parser = new FieldParser(appName);
      string[] args = new string[]
      {
        "-n",
        "fred",
        "-d"
      };

      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual("fred", parser.Name);
      Assert.IsTrue(parser.DoStuff);

      args = new string[]
      {
        "--name",
        "freddy",
        "--do-stuff"
      };

      parser.DoStuff = false;
      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual("freddy", parser.Name);
      Assert.IsTrue(parser.DoStuff);

      args = new string[]
      {
        "--name=fredrick",
        "--do-stuff=false"
      };

      Assert.IsTrue(parser.Parse(args));
      Assert.AreEqual("fredrick", parser.Name);
      Assert.IsFalse(parser.DoStuff);

      // Check that the help text contains the app name
      MemoryStream mem = new MemoryStream();
      StreamWriter sw = new StreamWriter(mem);
      sw.AutoFlush = true;
      TextWriter oldWriter = System.Console.Out;
      System.Console.SetOut(sw);
      parser.PrintHelp();
      System.Console.SetOut(oldWriter);
      mem.Seek(0, SeekOrigin.Begin);
      StreamReader sr = new StreamReader(mem);
      string helpText = sr.ReadToEnd();
      Assert.IsTrue(helpText.Contains(appName));
    }
  }
}
