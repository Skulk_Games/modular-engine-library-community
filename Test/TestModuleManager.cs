﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2022  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using NUnit.Framework;
using Skulk.MEL;
using Skulk.Test.Utils;
using System.Collections.Generic;

namespace Skulk.Test
{
  /// <summary>
  /// Tests for the <see cref="ModuleManager"/> class.
  /// </summary>
  public class TestModuleManager
  {
    /// <summary>
    /// Test adding and removing single module types to the <see cref="ModuleManager"/>
    /// </summary>
    [Test]
    public void AddRemoveModule()
    {
      ModuleManager manager = new ModuleManager();
      UpdateCounterModule counter = new UpdateCounterModule();
      TestModule test = new TestModule();

      // Add
      Assert.IsTrue(manager.AddModule(counter));
      Assert.AreEqual(counter, manager.GetModule<UpdateCounterModule>());

      // Adding same instance multiple times should fail
      Assert.IsFalse(manager.AddModule(counter));

      // Remove
      Assert.IsTrue(manager.RemoveModule(counter));
      Assert.IsNull(manager.GetModule<UpdateCounterModule>());

      // Add again
      Assert.IsTrue(manager.AddModule(counter));
      Assert.AreEqual(counter, manager.GetModule<UpdateCounterModule>());

      // Add another type
      Assert.IsTrue(manager.AddModule(test));
      Assert.AreEqual(test, manager.GetModule<TestModule>());
      Assert.AreEqual(2, manager.Modules.Count);

      // Remove the first one
      Assert.IsTrue(manager.RemoveModule(counter));
      Assert.IsNull(manager.GetModule<UpdateCounterModule>());
      Assert.AreEqual(test, manager.GetModule<TestModule>());
      Assert.AreEqual(1, manager.Modules.Count);

      // Remove the other
      Assert.IsTrue(manager.RemoveModule(test));
      Assert.IsNull(manager.GetModule<TestModule>());
      Assert.AreEqual(0, manager.Modules.Count);
    }

    /// <summary>
    /// Test adding and removing multiple modules of the same type
    /// </summary>
    [Test]
    public void AddRemoveMultiple()
    {
      UpdateCounterModule[] counters = new UpdateCounterModule[]
      {
        new UpdateCounterModule(),
        new UpdateCounterModule(),
        new UpdateCounterModule()
      };
      TestModule[] tests = new TestModule[]
      {
        new TestModule(),
        new TestModule()
      };

      ModuleManager manager = new ModuleManager();

      // Add all counters
      for (int i = 0; i < counters.Length; ++i)
      {
        Assert.IsTrue(manager.AddModule(counters[i]));
        Assert.AreEqual(manager.GetModules(typeof(UpdateCounterModule))[i], counters[i]);
      }

      // Add all tests
      for (int i = 0; i < tests.Length; ++i)
      {
        Assert.IsTrue(manager.AddModule(tests[i]));
        Assert.AreEqual(manager.GetModules(typeof(TestModule))[i], tests[i]);
      }

      // Clear all counters
      Assert.IsTrue(manager.RemoveModule<UpdateCounterModule>());
      Assert.IsNull(manager.GetModule<UpdateCounterModule>());

      // Add all counters again
      for (int i = 0; i < counters.Length; ++i)
      {
        Assert.IsTrue(manager.AddModule(counters[i]));
        Assert.AreEqual(counters[0], manager.GetModule<UpdateCounterModule>());
        Assert.AreEqual(manager.GetModules(typeof(UpdateCounterModule))[i], counters[i]);
      }

      // Clear tests
      Assert.IsTrue(manager.RemoveModule(typeof(TestModule)));
      Assert.IsNull(manager.GetModule<TestModule>());

      // Add all tests again
      for (int i = 0; i < tests.Length; ++i)
      {
        Assert.IsTrue(manager.AddModule(tests[i]));
        Assert.AreEqual(tests[0], manager.GetModule<TestModule>());
        Assert.AreEqual(manager.GetModules(typeof(TestModule))[i], tests[i]);
      }

      // Clear all counters
      Assert.IsTrue(manager.RemoveModule(typeof(UpdateCounterModule).AssemblyQualifiedName));
      Assert.IsNull(manager.GetModule<UpdateCounterModule>());

      // Add all counters again
      for (int i = 0; i < counters.Length; ++i)
      {
        Assert.IsTrue(manager.AddModule(counters[i]));
        Assert.AreEqual(counters[0], manager.GetModule<UpdateCounterModule>());
        Assert.AreEqual(manager.GetModules(typeof(UpdateCounterModule))[i], counters[i]);
      }

      // Get all modules
      IEnumerable<IModule> modules = manager.GetModules();
      int updateMods = 0;
      int testMods = 0;
      foreach (IModule mod in modules)
      {
        if (mod.GetType() == typeof(UpdateCounterModule))
        {
          ++updateMods;
          bool found = false;
          for (int i = 0; i < counters.Length; ++i)
          {
            if (counters[i] == mod)
            {
              found = true;
              break;
            }
          }

          Assert.IsTrue(found);
        }
        else
        {
          ++testMods;
          bool found = false;
          for (int i = 0; i < tests.Length; ++i)
          {
            if (tests[i] == mod)
            {
              found = true;
              break;
            }
          }

          Assert.IsTrue(found);
        }
      }

      Assert.AreEqual(counters.Length, updateMods);
      Assert.AreEqual(tests.Length, testMods);
    }
  }
}
