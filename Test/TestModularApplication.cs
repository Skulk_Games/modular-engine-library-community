﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using System;
using System.Threading;
using Skulk.MEL;
using NUnit.Framework;

namespace Skulk.Test
{
  [TestFixture]
  public class TestModularApplication
  {
    class TestApp : ModularApplication
    {
      public int m_loopCount = 0;

      public TestApp() : base()
      {
        // Empty
      }

      public TestApp(double maxUpdateHz, ILogger log = null, IUpdateGenerator updateGenerator = null) : base(maxUpdateHz, log, updateGenerator)
      {
        // Empty
      }

      protected override void Run()
      {
        while (!StopRun)
        {
          m_loopCount++;
          Thread.Sleep(100);
        }
      }
    }

    class TestModule : Module
    {
      private int m_updateCount;

      public int UpdateCount { get => m_updateCount; set => m_updateCount = value; }

      public override void Update(UpdateTime updateTime)
      {
        ++m_updateCount;
      }
    }

    [Test]
    public void Running()
    {
      TestApp app = new TestApp();
      Assert.AreEqual(0, app.m_loopCount);
      app.Start();
      Thread.Sleep(500);
      Assert.IsTrue(app.m_loopCount > 0);
      app.Stop();
      app.Join();
      int count = app.m_loopCount;
      Thread.Sleep(500);
      Assert.AreEqual(count, app.m_loopCount);
    }

    [Test]
    public void AddModule()
    {
      ModularApplication app = new ModularApplication();
      StaticLog log = new StaticLog(app);
      app.AddModule(log);
      Assert.AreEqual(log, app.GetModule<StaticLog>());
    }

    [Test]
    public void Updates()
    {
      ModularApplication app = new ModularApplication();
      TestModule mod = new TestModule();
      app.AddModule(mod);
      Assert.AreEqual(0, mod.UpdateCount);
      app.Start();
      Thread.Sleep(500);
      Assert.IsTrue(mod.UpdateCount > 0);
      app.Stop();
      app.Join();

      app = new ModularApplication(100);
      mod = new TestModule();
      app.AddModule(mod);
      Assert.AreEqual(0, mod.UpdateCount);
      app.Start();
      Thread.Sleep(500);
      Assert.IsTrue(mod.UpdateCount > 0);
      app.Stop();
      app.Join();
    }
  }
}
