﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2022  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using Skulk.MEL;
using System.Collections.Concurrent;

namespace Skulk.Test.Utils
{
  public class TestModule : Module
  {
    private int m_initCount;
    private int m_refreshCount;
    private int m_startCount;
    private int m_stopCount;
    private int m_updateCount;
    private ConcurrentQueue<int> m_startedEvents = new ConcurrentQueue<int>();
    private ConcurrentQueue<int> m_endedEvents = new ConcurrentQueue<int>();

    public int InitCount { get => m_initCount; }
    public int RefreshCount { get => m_refreshCount; }
    public int StartCount { get => m_startCount; }
    public int StopCount { get => m_stopCount; }
    public int UpdateCount { get => m_updateCount; }
    public ConcurrentQueue<int> StartedEvents { get => m_startedEvents; }
    public ConcurrentQueue<int> EndedEvents { get => m_endedEvents; }

    public override void ModuleStartup()
    {
      base.ModuleStartup();
      ++m_startCount;
    }

    public override void ModuleShutdown()
    {
      base.ModuleShutdown();
      ++m_stopCount;
    }

    public override void EventStart(int turn)
    {
      base.EventStart(turn);
      m_startedEvents.Enqueue(turn);
    }

    public override void EventEnd(int turn)
    {
      base.EventEnd(turn);
      m_endedEvents.Enqueue(turn);
    }

    public override void ModuleInit()
    {
      base.ModuleInit();
      ++m_initCount;
    }

    public override void Refresh()
    {
      base.Refresh();
      ++m_refreshCount;
    }

    public override void Update(UpdateTime updateTime)
    {
      base.Update(updateTime);
      ++m_updateCount;
    }
  }
}
