﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2018  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using NUnit.Framework;
using Skulk.MEL;
using Skulk.MEL.Utils;
using System.Collections.Concurrent;

namespace Skulk.Test
{
  /// <summary>
  /// Tests the <see cref="TimeoutQueue{T}"/> class.
  /// </summary>
  public class TestTimeoutQueue
  {
    private ConcurrentQueue<string> m_expiredNames = new ConcurrentQueue<string>();
    private PeriodicUpdateGenerator m_generator = new PeriodicUpdateGenerator(30);

    /// <summary>
    /// Tear down after each test.
    /// </summary>
    [TearDown]
    public void TearDown()
    {
      m_generator.Stop();
      m_expiredNames.Clear();
    }

    /// <summary>
    /// Test the methods of the <see cref="TimeoutQueue{T}"/> class with a single item.
    /// </summary>
    [Test]
    public void SingleItem()
    {
      TimeoutQueue<string> names = new TimeoutQueue<string>();
      names.OnItemExpired += HandleItemExpired;
      m_generator.OnUpdate += names.Update;
      string name = "joe";
      m_generator.Start(true);

      // Enqueue and expire
      names.Enqueue(name, 0.1);
      Assert.AreEqual(name, names.Peek());
      Assert.AreEqual(1, names.Count);
      System.Threading.Thread.Sleep(50);
      Assert.AreEqual(1, names.Count);
      System.Threading.Thread.Sleep(100);
      Assert.AreEqual(0, names.Count);
      Assert.IsNull(names.Peek());
      Assert.AreEqual(1, m_expiredNames.Count);
      Assert.IsTrue(m_expiredNames.TryDequeue(out string expiredName));
      Assert.AreEqual(name, expiredName);

      // Enqueue and dequeue
      names.Enqueue(name, 0.1);
      Assert.AreEqual(name, names.Peek());
      Assert.AreEqual(1, names.Count);
      System.Threading.Thread.Sleep(50);
      string dequeuedName = names.Dequeue();
      Assert.IsNotNull(dequeuedName);
      Assert.AreEqual(name, dequeuedName);
      Assert.AreEqual(0, names.Count);

      // Reset
      names.Enqueue(name, 0.1);
      Assert.AreEqual(name, names.Peek());
      Assert.AreEqual(1, names.Count);
      for (int i = 0; i < 10; ++i)
      {
        names.Reset();
        System.Threading.Thread.Sleep(25);
        Assert.AreEqual(1, names.Count);
      }
      Assert.AreEqual(name, names.Peek());
      Assert.AreEqual(1, names.Count);

      // ToArray
      string[] nameArray = names.ToArray();
      Assert.IsNotNull(nameArray);
      Assert.AreEqual(1, nameArray.Length);
      Assert.AreEqual(name, nameArray[0]);
      Assert.AreEqual(name, names.Dequeue());
      nameArray = names.ToArray();
      Assert.IsNotNull(nameArray);
      Assert.AreEqual(0, nameArray.Length);
    }

    /// <summary>
    /// Handle an expired item in the queue.
    /// </summary>
    /// <param name="item">The item that expired.</param>
    private void HandleItemExpired(string item)
    {
      m_expiredNames.Enqueue(item);
    }
  }
}
