﻿// Modular Engine Library(MEL) a library and framework for making modular applications.
//  Copyright(C) 2022  Ryan Steven Jaynes
// 
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with this program.If not, see<https://www.gnu.org/licenses/>.

using NUnit.Framework;
using Skulk.MEL;
using Skulk.MEL.Utils;
using System.Collections.Concurrent;

namespace Skulk.Test
{
  /// <summary>
  /// Tests the <see cref="TimeoutList{T}"/> class.
  /// </summary>
  public class TestTimeoutList
  {
    private ConcurrentQueue<string> m_expiredItems = new ConcurrentQueue<string>();
    private PeriodicUpdateGenerator m_generator = new PeriodicUpdateGenerator(30);

    /// <summary>
    /// Tears down after each test.
    /// </summary>
    [TearDown]
    public void TearDown()
    {
      m_expiredItems.Clear();
      m_generator.Stop();
    }

    /// <summary>
    /// Test the methods of the <see cref="TimeoutList{T}"/> class with a single item.
    /// </summary>
    [Test]
    public void SingleItem()
    {
      TimeoutList<string> names = new TimeoutList<string>();
      names.OnItemExpired += HandleItemExpired; 
      m_generator.OnUpdate += names.Update;
      m_generator.Start();
      string name = "fred";

      // Add and expire
      names.Add(name, 0.1);
      Assert.AreEqual(0, m_expiredItems.Count);
      System.Threading.Thread.Sleep(50);
      Assert.IsTrue(names.Contains(name));
      System.Threading.Thread.Sleep(100);
      Assert.IsFalse(names.Contains(name));
      Assert.AreEqual(1, m_expiredItems.Count);
      Assert.IsTrue(m_expiredItems.TryDequeue(out string expiredName));
      Assert.AreEqual(name, expiredName);

      // Add and remove
      names.Add(name, 0.1);
      Assert.IsTrue(names.Contains(name));
      Assert.AreEqual(name, names.Get(0));
      Assert.AreEqual(name, names[0]);
      System.Threading.Thread.Sleep(50);
      Assert.IsTrue(names.Contains(name));
      Assert.AreEqual(name, names[0]);
      Assert.AreEqual(1, names.Count);
      Assert.IsTrue(names.Remove(name));
      Assert.IsFalse(names.Remove(name));
      Assert.AreEqual(0, names.Count);

      // Find
      names.Add(name, 0.1);
      Assert.AreEqual(name, names.Find(x => x.Contains(name)));
      Assert.IsNull(names.Find(x => x.Equals("123")));

      // FindIndex
      Assert.AreEqual(0, names.FindIndex(x => x.Equals(name)));
      Assert.AreEqual(-1, names.FindIndex(x => x.Equals("123")));

      // RemoveAt
      names.RemoveAt(0);
      Assert.AreEqual(0, names.Count);
      Assert.Throws(typeof(System.ArgumentOutOfRangeException), () => names.RemoveAt(0));

      // Reset
      names.Add(name, 0.1);
      for (int i = 0; i < 10; ++i)
      {
        names.Reset(0);
        System.Threading.Thread.Sleep(25);
      }

      Assert.AreEqual(1, names.Count);
      Assert.AreEqual(name, names[0]);

      // Reset an out of range index does nothing
      names.Reset(1);

      // ToArray
      string[] nameArray = names.ToArray();
      Assert.IsNotNull(nameArray);
      Assert.AreEqual(1, nameArray.Length);
      Assert.AreEqual(name, nameArray[0]);

      // Set
      string name2 = "freddy";
      names.Set(0, name2);
      Assert.AreEqual(name2, names[0]);
      names[0] = name;
      Assert.AreEqual(name, names[0]);

      // Setting invalid index does nothing
      names.Set(1, name2);
      names[1] = name2;
      Assert.AreEqual(1, names.Count);

      names.RemoveAt(0);
      nameArray = names.ToArray();
      Assert.IsNotNull(nameArray);
      Assert.AreEqual(0, nameArray.Length);
    }

    /// <summary>
    /// Handles the event invoked when an item expires in the list.
    /// </summary>
    /// <param name="item">The item that expired in the list.</param>
    private void HandleItemExpired(string item)
    {
      m_expiredItems.Enqueue(item);
    }
  }
}
