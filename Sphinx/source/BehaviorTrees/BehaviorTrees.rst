.. _BehaviorTrees:

Behavior Trees
===================

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Contents:
   :glob:

   SelectorNode
   SequenceNode
   RepeaterNode
   InverterNode
   SucceederNode

Behavior trees are a powerful way to organize and create reusable blocks of logic. They are often used when create AI logic for game enemies or characters.
With behavior trees the sky is the limit and they can be used and applied to anything at all.

A behavior tree consists of nodes that define the logical flow of the tree. Within MEL many different nodes have been implemented but the standard core nodes
typically include the following:

  * Selector Node
  * Sequence Node
  * Repeater Node
  * Inverter Node
  * Succeeder Node

These are fairly standard nodes you should be able to find in any behavior tree library. Each of these nodes can be added into a tree and supports child nodes.
Child nodes are executed according to the logic of the parent node on each ``Tick()`` of the tree.
