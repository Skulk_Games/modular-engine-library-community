.. _Examples:

Examples
----------------

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Contents:
   :glob:

   ExampleModule

We've included some example code in this documentation to help you understand simple ways the library and framework can be used.
More detailed examples would be to look at source code for larger projects that implement MEL. As we become aware of its use in other
projects we'll link to more examples here.

Simple Examples
-----------------

  * :ref:`ExampleModule`

Project Examples
-----------------

  * `Juicer <https://skulkgames.com>`_
