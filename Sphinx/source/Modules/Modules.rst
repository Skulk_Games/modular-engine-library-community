.. _Modules:

Modules
=====================

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Contents:
   :glob:

   ClickerPlugin
   VisualNovelPlugin
   WorldMapPlugin

To make the most out of MEL becoming familiar with the concept of the Module is essential.

The Module<T> Class
--------------------

The ``Module<T>`` class is the base class with the ``ModularApplication`` framework and all custom modules should derive from this class.
