.. _VisualNovelPlugin:

Visual Novel Plugin
=====================

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Contents:
   :glob:

   VisualNovel/VisualNovelXml

The visual novel plugin is designed to perform actions related to displaying dialogue and stepping through pages
and scenes that include text, images, characters and the modifying of variables.

The plugin itself isn't always necessary as the ``VisualNovelRunner`` is the class that actually traverses through
a ``VisualNovelRoot`` class that describes all the scenes and pages within a novel.

Usage
--------

When it comes to implementing a visual novel the typical flow is as follows:

  #. Create the visual novel XML file describing all the scenes, pages, choices, and variables in the novel
  #. Implement custom code that provides additional functionality (Saving, Loading, Derived variables
  #. Include the backend in GUI code


:ref:`VisualNovelXml`
----------------------

The Visual Novel XML file is used to define the contents of the novel outside of code and allows a novel to be loaded at runtime.
