.. _WorldMapPlugin:

World Map Plugin
=====================

The world map plugin implements the logic and methods used for working with a world map containing objects.
