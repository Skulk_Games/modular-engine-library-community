.. _VisualNovelXml:

Visual Novel XML
====================

The ``VisualNovelPlugin`` makes use of an XML file to more easily allow the novel flow to be defined outside of code.
This page serves as a reference for the format of the XML file.

Novel
--------

The main tag for the XML is the ``<Novel>`` tag:

.. code-block:: xml

  <Novel xmlns="https://www.skulkgames.com/schema"
   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xsi:schemaLocation="https://www.skulkgames.com/schema https://www.skulkgames.com/schema/VisualNovel.xsd">

Like any XML file this tag is where the schema information is defined as shown above. This tag does not expect any other attributes.


Characters
++++++++++++++++

Within the ``<Novel>`` tag the characters used within the novel can be defined in within the ``<Characters>`` tag.
This tag consists of a collection of ``<Character>`` tags

Character
^^^^^^^^^^^^^

  The ``<Character>`` tag includes three attributes

  :id: An integer specifying a unique number to associate with this character

  :name: A unique name for referencing this character

  :imageId: A unique integer ID that is associated with the image used to portray this character

  .. code-block:: xml

    <Character id="0" name="Roommate" imageId="0"/>

Variables
++++++++++++

