.. _ModularEngineLibrary:

Modular Engine Library (MEL)
==================================================

.. toctree::
   :hidden:
   :maxdepth: 1
   :caption: Contents:

   Modules/Modules
   Applications/Applications
   Examples/Examples
   BehaviorTrees/BehaviorTrees
   Characters/Characters
   Coords/Coords
   Items/Items
   Logging/Logging
   ModularObject/ModularObject

The modular engine library is a collection of classes that is designed to provide the functionality
that is commonly needed when developing the backend (engine) for games.

MEL is built around the concept of modular applications that allow new functionality to be implemented in modules.
Modules are able to interract with other modules added on an application to allow for dependencies and complex interactions.
While the ``ModularApplication`` is the namesake of the library there are multiple other application classes that can be used for easily developing threaded applications.

MEL is primarily a library of classes that can be implemented anywhere but the ``ModularApplication`` is implemented as a framework that calls module methods.
A module's methods can be overridden to customize their behavior within the application framework.

Overview
=================

:ref:`Modules`
-----------------

A module refers to any class that derives from the ``Module<T>`` class.
Within MEL the modules that are meant to provide a library of game engine functionality are called plugins.
These are functionally no different from modules and the name only helps to distinguish modules that are meant
to act as a core part of a game engine from other more portable modules.

:ref:`Applications`
--------------------

Applications include the classes that run their own threads to perform some form of processing.

Getting Started
===================

The easiest way to get started using MEL is to download and install the NuGet package Skulk.MEL, this is available on nuget.org.
