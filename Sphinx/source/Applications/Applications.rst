.. _Applications:

Applications
================

There are an assortment of different application classes within MEL.
But they all share the same general idea of starting a new thread and performing
some processing in a run method before finishing or being stopped.
