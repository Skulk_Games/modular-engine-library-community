# MEL Community Version

## Modular Engine Library (MEL)

Provides a library and framework for creating modular applications.

This is the open source community version of one of my hobby project libraries.
This code library and framework is designed to help me develop threaded applications
with simple built in logging and command line parsing without me needing to reference
other separate projects.

The most flexible feature of the library is the ModularApplication class which is where the
project gets its name. This class allows application functionality to be defined by modules
that are called by the application framework.

## Documentation

The documentation for this project is still a work in progress. The Sphinx folder contains some initial documentation
that I'm working on to make a sort of user's guide. I'll update it more as I have time, which means slowly. Sorry.

## Copyright and Attributions

Unless otherwise specified (C) 2018 Ryan Steven Jaynes

